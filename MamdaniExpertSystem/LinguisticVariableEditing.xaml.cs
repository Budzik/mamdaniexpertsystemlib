﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OxyPlot;
using OxyPlot.Series;
using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions;
using MamdaniExpertSystemLib;

namespace MamdaniExpertSystem
{

    public partial class LinguisticVariableEditing : Window
    {        private LinguisticVariable linguisticVariable;
        public EventHandler OnCloseEvent;
        public LinguisticVariableEditing(LinguisticVariable linguisticVariable)
        {
            this.linguisticVariable = linguisticVariable;
            InitializeComponent();
            string Title = linguisticVariable.Name;
            GiveItemsInCombobox();
            RefreshPlot();
            RefreshMembershipFunctionListAndLimits();
        }
        private void GiveItemsInCombobox()
        {
            List<string> temp;
            temp = MembershipFunctionFactory.GiveAvailableTypeOfMembershipFunctions();
            TypeOfFunction.ItemsSource = temp;
        }

        private void ChangeSelection(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                MembershipFunctionName.Text = linguisticVariable.MembershipFunctions[MembershipFunctionList.SelectedIndex].Name;
                Parameters.Text = ParametersVector.TextFromParameters(linguisticVariable.MembershipFunctions[MembershipFunctionList.SelectedIndex].Parameters);
                TypeOfFunction.SelectedIndex = MembershipFunctionFactory.GetNumberOfFunction(linguisticVariable.MembershipFunctions[MembershipFunctionList.SelectedIndex].TypeOfMembershipFunction);
            }
            catch
            {

            }
        }

        private void AddMembershipFunction(object sender, RoutedEventArgs e)
        {
            try
            {
                TypeOfMembershipFunctions membershipFunction = (TypeOfMembershipFunctions)TypeOfFunction.SelectedIndex;
                List<double> parameters = ParametersVector.ParametersFromText(Parameters.Text);
                string name = MembershipFunctionName.Text;
                MembershipFunction exampleFunction = MembershipFunctionFactory.CreateMembershipFunction(membershipFunction, parameters, name);
                linguisticVariable.AddMembershipFunction(exampleFunction);
                RefreshMembershipFunctionListAndLimits();
                RefreshPlot();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void DeleteMembershipFunction(object sender, RoutedEventArgs e)
        {
            try
            {
                linguisticVariable.RemoveMembershipFunction(MembershipFunctionList.SelectedIndex);
                RefreshMembershipFunctionListAndLimits();
                RefreshPlot();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EditMembershipFunction(object sender, RoutedEventArgs e)
        {
            try
            {
                TypeOfMembershipFunctions membershipFunction = (TypeOfMembershipFunctions)TypeOfFunction.SelectedIndex;
                List<double> parameters = ParametersVector.ParametersFromText(Parameters.Text);
                string name = MembershipFunctionName.Text;
                MembershipFunction exampleFunction = MembershipFunctionFactory.CreateMembershipFunction(membershipFunction, parameters, name);
                int number = MembershipFunctionList.SelectedIndex;
                if (number != -1) {
                    linguisticVariable.EditMembershipFunction(number, exampleFunction);
                }
                RefreshMembershipFunctionListAndLimits();
                RefreshPlot();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void RefreshMembershipFunctionListAndLimits()
        {
            List<string> temp = new List<string>();
            foreach(MembershipFunction membershipFunction in linguisticVariable.MembershipFunctions)
            {
                temp.Add(membershipFunction.Name);
            }
            MembershipFunctionList.ItemsSource = temp;
            LowerLimit.Text = linguisticVariable.LowerLimitOfRange.ToString();
            UpperLimit.Text = linguisticVariable.UpperLimitOfRange.ToString();
            MembershipFunctionList.SelectedIndex = -1;
        }
        private void RefreshPlot()
        {
            PlotModel plotModel = new PlotModel();
            
            double interval = (linguisticVariable.UpperLimitOfRange - linguisticVariable.LowerLimitOfRange) * 0.01 / 10;
            foreach (MembershipFunction membershipFunction in linguisticVariable.MembershipFunctions)
            {
                plotModel.Series.Add(new FunctionSeries(membershipFunction.DesignateMembershipGradeOfFuzzySet, linguisticVariable.LowerLimitOfRange, linguisticVariable.UpperLimitOfRange, interval , membershipFunction.Name));
            }
            
            ActualPlot.Model = plotModel;
        }

        private void LowerLimitChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                linguisticVariable.LowerLimitOfRange = double.Parse(LowerLimit.Text);
                RefreshPlot();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpperLimitChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                linguisticVariable.UpperLimitOfRange = double.Parse(UpperLimit.Text);
                RefreshPlot();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            OnCloseEvent(linguisticVariable, e);
        }

        private void Parameters_Focus(object sender, RoutedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if(tb.Text.Length == 0)
            {
                tb.Text = "[]";
                tb.SelectionStart = 0;
                tb.SelectionLength = 0;
            }
        }
    }
}
