﻿using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MamdaniExpertSystem
{
    public partial class MainWindow : Window
    {
        private readonly string[] InputSeparators = new string[] { " ", "(", ")", "=" };
        private readonly string[] OutputSeparators = new string[] { ", ", ")", " ", "=" };
        private readonly string[] IfthenSepatators = new string[] { "if(", ")then(" };
        private readonly string[] CursorSeparator = new string[] { "###balabala###!@#" };
        private Key previousKey;
        private MamdaniExpertSystemLib.MamdaniExpertSystem mamdaniExpertSystem;
        public MainWindow()
        {            
            mamdaniExpertSystem = new MamdaniExpertSystemLib.MamdaniExpertSystem();
            InitializeComponent();
            TNorm.ItemsSource = TwoArgumentsOperatorGenerator.GiveAllAvailableTNorms();
            SNorm.ItemsSource = TwoArgumentsOperatorGenerator.GiveAllAvailableSNorms();
            Implication.ItemsSource = TwoArgumentsOperatorGenerator.GiveAllAvailableTNorms();
            Aggregation.ItemsSource = TwoArgumentsOperatorGenerator.GiveAllAvailableSNorms();
            RefreshProperties();
            this.KeyDown += CtrlAndEnter_Click;
            
        }
        
        private void CtrlAndEnter_Click(object sender, KeyEventArgs e)
        {
            //TODO: weź użyj tego previous keya, powinno kurwa działać
            if (RuleExpression.IsFocused)
            {
                if(previousKey == Key.LeftCtrl)
                {                  
                    if(e.Key == Key.LeftAlt)
                    {
                        int SelectionStart = RuleExpression.SelectionStart;
                        string afterCursorText = RuleExpression.Text.Substring(SelectionStart);
                        string beforeCursorText = RuleExpression.Text.Remove(RuleExpression.Text.Length - afterCursorText.Length, afterCursorText.Length);
                        try
                        {
                            string[] s = beforeCursorText.Split(IfthenSepatators, StringSplitOptions.RemoveEmptyEntries);
                            bool b = false;
                            if (s.Length == 1)
                            {
                                b = true;
                            }
                            else if (s.Length == 2)
                            {
                                b = false;
                            }
                            else
                            {
                                throw new ArgumentException("Rule expression is not correct.");
                            }
                            if (b == true)
                            {
                                string text;
                                string[] s1 = s[0].Split(InputSeparators, StringSplitOptions.RemoveEmptyEntries);
                                text = s1[s1.Length - 1];
                                int l1 = text.Length;
                                bool lvFlag = true;
                                int l = s1.Length;
                                if(l > 1)
                                {
                                    if(mamdaniExpertSystem.FindInputByName(s1[l - 2]) != -1)
                                    {
                                        lvFlag = false;
                                    }
                                }
                                if (lvFlag)
                                {
                                    s1[l - 1] = mamdaniExpertSystem.CompleteLinguisticVariable(s1[l - 1], b);
                                    if (s1[l - 1] != null)
                                    {
                                        string[] sss = afterCursorText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                        if (sss.Length > 0)
                                        {
                                            if (!sss[0].Equals("="))
                                            {
                                                s1[l - 1] += " = ";
                                            }
                                        }
                                        else
                                        {
                                            s1[l - 1] += " = ";
                                        }
                                    }
                                }
                                else
                                {
                                    s1[l - 1] = mamdaniExpertSystem.CompleteMembershipFunction(s1[l - 1], s1[l - 2], b);
                                }
                                if (s1[l - 1] != null)
                                {
                                    beforeCursorText = beforeCursorText.Remove(beforeCursorText.Length - l1, l1);
                                    beforeCursorText += s1[l - 1];
                                }                        
                            }
                            else if (b == false)
                            {
                                string text;
                                string[] s1 = s[1].Split(OutputSeparators, StringSplitOptions.RemoveEmptyEntries);
                                text = s1[s1.Length - 1];
                                int l1 = text.Length;
                                bool lvFlag = true;
                                int l = s1.Length;
                                if (l > 1)
                                {
                                    if (mamdaniExpertSystem.FindOutputByName(s1[l - 2]) != -1)
                                    {
                                        lvFlag = false;
                                    }
                                }
                                if (lvFlag)
                                {
                                    s1[l - 1] = mamdaniExpertSystem.CompleteLinguisticVariable(s1[l - 1], b);
                                    if (s1[l - 1] != null)
                                    {
                                        string[] sss = afterCursorText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                        if (sss.Length > 0)
                                        {
                                            if (!sss[0].Equals("="))
                                            {
                                                s1[l - 1] += " = ";
                                            }
                                        }
                                        else
                                        {
                                            s1[l - 1] += " = ";
                                        }
                                    }
                                }
                                else
                                {
                                    s1[l - 1] = mamdaniExpertSystem.CompleteMembershipFunction(s1[l - 1], s1[l - 2], b);
                                }
                                if (s1[l - 1] != null)
                                {
                                    beforeCursorText = beforeCursorText.Remove(beforeCursorText.Length - l1, l1);
                                    beforeCursorText += s1[l - 1];
                                }
                            }
                            RuleExpression.Text = beforeCursorText + afterCursorText;
                            RuleExpression.SelectionStart = beforeCursorText.Length;
                            RuleExpression.SelectionLength = 0;
                        }
                        catch (ArgumentException ex)
                        {
                            
                        }
                    }
                }
                if(previousKey == Key.LeftCtrl && e.Key == Key.T)
                {
                    RuleExpression.SelectionStart = SetCursor(RuleExpression.Text, RuleExpression.SelectionStart);
                }
                previousKey = e.Key;
            }
        }
        private int SetCursor(string text, int position)
        {
            text = text.ToLower();
            string[] splitedText = text.Split(new string[] { "then" }, StringSplitOptions.RemoveEmptyEntries);
            if(splitedText.Length == 2)
            {
                if(position < splitedText[0].Length)
                {
                    return text.Length - 1;
                }
                else
                {
                    return splitedText[0].Length - 1;
                }
            }
            else
            {
                return 0;
            }

        }
        private void CompleteTheWord()
        {
            string[] s = RuleExpression.Text.Split(new string[] { "=", " " }, StringSplitOptions.RemoveEmptyEntries);
        }
        private void AddInput(object sender, RoutedEventArgs e)
        {
            try
            {
                LinguisticVariable linguisticVariable = new LinguisticVariable();
                linguisticVariable.Name = InputName.Text;
                mamdaniExpertSystem.AddInput(linguisticVariable);
                this.RefreshInputList();
                LinguisticVariableEditing window = new LinguisticVariableEditing(linguisticVariable);
                window.Show();
                window.OnCloseEvent += WindowClose;
            }            
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void WindowClose(object sender, EventArgs e)
        {
            RefreshRules();
            RefreshProperties();
            RefreshInputList();
            RefreshOutputList();
        }
        private void DeleteInput(object sender, RoutedEventArgs e)
        {
            try
            {
                string temp = (string)InputList.SelectedItem;
                mamdaniExpertSystem.RemoveInput(temp);
                RefreshInputList();
                RefreshRules();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void RefreshInputList()
        {
            List<string> temp = new List<string>();
            foreach(LinguisticVariable linguisticVariable in mamdaniExpertSystem.Inputs)
            {
                temp.Add(linguisticVariable.Name);
            }
            InputList.ItemsSource = temp;
        }
        private void RefreshOutputList()
        {
            List<string> temp = new List<string>();
            foreach (LinguisticVariable linguisticVariable in mamdaniExpertSystem.Outputs)
            {
                temp.Add(linguisticVariable.Name);
            }
            OutputList.ItemsSource = temp;
        }
        private void DeleteOutput(object sender, RoutedEventArgs e)
        {
            try
            {
                string temp = (string)OutputList.SelectedItem;
                mamdaniExpertSystem.RemoveOutput(temp);
                RefreshOutputList();
                RefreshRules();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void EditOutput(object sender, RoutedEventArgs e)
        {
            try
            {
                string temp = (string)OutputList.SelectedItem;
                LinguisticVariable linguisticVariable = mamdaniExpertSystem.Outputs[mamdaniExpertSystem.FindOutputByName(temp)];
                LinguisticVariableEditing window = new LinguisticVariableEditing(linguisticVariable);
                window.Show();
                RefreshOutputList();
                window.OnCloseEvent += WindowClose;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void EditInput(object sender, RoutedEventArgs e)
        {
            try
            {
                string temp = (string)InputList.SelectedItem;
                LinguisticVariable linguisticVariable = mamdaniExpertSystem.Inputs[mamdaniExpertSystem.FindInputByName(temp)];
                LinguisticVariableEditing window = new LinguisticVariableEditing(linguisticVariable);
                window.Show();
                RefreshInputList();
                window.OnCloseEvent += WindowClose;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void AddOutput(object sender, RoutedEventArgs e)
        {
            try
            {
                LinguisticVariable linguisticVariable = new LinguisticVariable();
                linguisticVariable.Name = OutputName.Text;
                mamdaniExpertSystem.AddOutput(linguisticVariable);
                this.RefreshOutputList();
                LinguisticVariableEditing window = new LinguisticVariableEditing(linguisticVariable);
                
                window.Show();
                window.OnCloseEvent += WindowClose;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void DuplicateInput(object sender, RoutedEventArgs e)
        {
            try
            {
                LinguisticVariable exampleLinguisticVariable = mamdaniExpertSystem.Inputs[InputList.SelectedIndex];
                LinguisticVariable linguisticVariable = new LinguisticVariable(exampleLinguisticVariable, InputName.Text);
                mamdaniExpertSystem.Inputs.Add(linguisticVariable);
                RefreshInputList();
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void DuplicateOutput(object sender, RoutedEventArgs e)
        {
            try
            {
                LinguisticVariable exampleLinguisticVariable = mamdaniExpertSystem.Outputs[OutputList.SelectedIndex];
                LinguisticVariable linguisticVariable = new LinguisticVariable(exampleLinguisticVariable, OutputName.Text);
                mamdaniExpertSystem.Outputs.Add(linguisticVariable);
                RefreshOutputList();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void RefreshRules()
        {
            RulesList.ItemsSource = mamdaniExpertSystem.ruleSet.ShowAllRule();
        }
        private void AddRule(object sender, RoutedEventArgs e)
        {
            try
            {
                mamdaniExpertSystem.ruleSet.AddRule(RuleExpression.Text);
                RefreshRules();
                
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Rule expression is incorrect");
            }
        }
        private void RemoveRule(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RulesList.SelectedIndex >= 0)
                {
                    mamdaniExpertSystem.ruleSet.RemoveRule(RulesList.SelectedIndex);
                    RefreshRules();
                    
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Rule expression is incorrect");
            }
        }
        private void EditRule(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RulesList.SelectedIndex >= 0)
                {
                    mamdaniExpertSystem.ruleSet.EditRule(RuleExpression.Text, RulesList.SelectedIndex);
                    RefreshRules();
                    
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Rule expression is incorrect");
            }
        }
        private void RulesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RuleExpression.Text = (string)RulesList.SelectedItem;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Result window = new Result(mamdaniExpertSystem);

                window.Show();
            }
            catch
            {
                MessageBox.Show("Something is wrong. Try to check that inputs, outputs and rules is exist and they are correct.");
            }

        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MamdaniExpertSystemLib.MamdaniExpertSystemBuilder MESBuilder = new MamdaniExpertSystemLib.MamdaniExpertSystemBuilder(mamdaniExpertSystem);

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "xml file(*.xml)|*.xml";
                if (saveFileDialog.ShowDialog() == true)
                {
                    MESBuilder.SaveToFile(saveFileDialog.FileName);
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Load_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MamdaniExpertSystemLib.MamdaniExpertSystemBuilder MESBuilder;
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "xml file(*.xml)|*.xml";
                if (openFileDialog.ShowDialog() == true)
                {
                    MESBuilder = new MamdaniExpertSystemLib.MamdaniExpertSystemBuilder(openFileDialog.FileName);
                    mamdaniExpertSystem = MESBuilder.GenerateMamdaniExpertSystem();
                }
                RefreshInputList();
                RefreshOutputList();
                RefreshRules();
                RefreshProperties();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void RefreshProperties()
        {
            SNorm.SelectedIndex = TwoArgumentsOperatorGenerator.GiveNumberOfSnorm(mamdaniExpertSystem.ruleSet.SNorm.Name);
            TNorm.SelectedIndex = TwoArgumentsOperatorGenerator.GiveNumberOfTNorm(mamdaniExpertSystem.ruleSet.TNorm.Name);
            Implication.SelectedIndex = TwoArgumentsOperatorGenerator.GiveNumberOfTNorm(mamdaniExpertSystem.Implication.Name);
            Aggregation.SelectedIndex = TwoArgumentsOperatorGenerator.GiveNumberOfSnorm(mamdaniExpertSystem.ruleSet.Aggregation.Name);
            PrecissionTextBox.Text = mamdaniExpertSystem.precission.ToString();
            PrecissionSlider.Value = mamdaniExpertSystem.precission;

        }
        private void SNorm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mamdaniExpertSystem.ruleSet.SNorm = TwoArgumentsOperatorGenerator.GetSNorm((SNorms)TwoArgumentsOperatorGenerator.GiveNumberOfSnorm(SNorm.SelectedItem.ToString()));
        }
        private void TNorm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mamdaniExpertSystem.ruleSet.TNorm = TwoArgumentsOperatorGenerator.GetTNorm((TNorms)TwoArgumentsOperatorGenerator.GiveNumberOfTNorm(TNorm.SelectedItem.ToString()));
        }
        private void Implication_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mamdaniExpertSystem.Implication = TwoArgumentsOperatorGenerator.GetTNorm((TNorms)TwoArgumentsOperatorGenerator.GiveNumberOfTNorm(Implication.SelectedItem.ToString()));
        }
        private void Aggregation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mamdaniExpertSystem.ruleSet.Aggregation = TwoArgumentsOperatorGenerator.GetSNorm((SNorms)TwoArgumentsOperatorGenerator.GiveNumberOfSnorm(Aggregation.SelectedItem.ToString()));
        }
        private void PrecissionSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                mamdaniExpertSystem.precission = (int)PrecissionSlider.Value;

                PrecissionTextBox.Text = mamdaniExpertSystem.precission.ToString();
            }
            catch
            {

            }
            
        }
        private void PrecissionTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int value;

                if(int.TryParse(PrecissionTextBox.Text, out value))
                {
                    mamdaniExpertSystem.precission = value;
                    PrecissionSlider.Value = value;
                }

                
            }
            catch
            {

            }

        }
        private void RenameInput(object sender, RoutedEventArgs e)
        {
            string s = InputName.Text;
            mamdaniExpertSystem.Inputs[InputList.SelectedIndex].Name = s;
            RefreshInputList();
            RefreshRules();
        }
        private void RenameOutput(object sender, RoutedEventArgs e)
        {
            string s = OutputName.Text;
            mamdaniExpertSystem.Outputs[OutputList.SelectedIndex].Name = s;
            RefreshOutputList();
            RefreshRules();
        }
        private void AddIfText(object sender, RoutedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if(tb.Text.Length == 0)
            {
                tb.Text = "if()then()";
                tb.SelectionStart = 3;
                tb.SelectionLength = 0;
            }
            
        }
        private void New_Click(object sender, RoutedEventArgs e)
        {
            string mst = "If project is don't saved you will lose your data, do you want to continue? ";
            string msm = "Warning!";
            MessageBoxButton msb = MessageBoxButton.YesNo;
            MessageBoxImage msi = MessageBoxImage.Warning;
            MessageBoxResult rsltMessageBox = MessageBox.Show(mst, msm, msb, msi);
            switch (rsltMessageBox)
            {
                case MessageBoxResult.Yes:
                    mamdaniExpertSystem = new MamdaniExpertSystemLib.MamdaniExpertSystem();
                    RefreshInputList();
                    RefreshOutputList();
                    RefreshProperties();
                    RefreshRules();
                    break;
                case MessageBoxResult.No:
                    break;
                default:
                    break;
            }
        }
    }
}
