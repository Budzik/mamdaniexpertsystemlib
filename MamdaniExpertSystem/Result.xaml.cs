﻿using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MamdaniExpertSystem
{
    /// <summary>
    /// Interaction logic for Result.xaml
    /// </summary>
    public partial class Result : Window
    {
        int index = 0;
        List<RadioButton> radioButtonsList;
        List<TextBox> textBoxList;
        List<Slider> slidersList;
        private MamdaniExpertSystemLib.MamdaniExpertSystem MES;
        private Label result;

        public Result(MamdaniExpertSystemLib.MamdaniExpertSystem MES)
        {
            result = new Label();
            radioButtonsList = new List<RadioButton>();
            textBoxList = new List<TextBox>();
            slidersList = new List<Slider>();
            this.MES = MES;
            InitializeComponent();
            AddSliders();
            AddRadioBoxes();
            int i = 0;
            foreach (Slider tb in slidersList)
            {
                double temp = (MES.Inputs[i].LowerLimitOfRange + MES.Inputs[i].UpperLimitOfRange) / 2;
                tb.Value = temp;
                i++;
            }
            CalculateValue();
            RefreshPlot();
            radioButtonsList[0].IsChecked = true;

        }
        private void CalculateValue()
        {
            if (result.Content == null)
            {
                result.Content = "Result: " + MES.CalculateValue(getArguments())[index].ToString();
                result.Foreground = Brushes.White;
                Outputs.Children.Add(result);
                Grid.SetRow(result, Outputs.RowDefinitions.Count - 1);
            }
            else
            {
                result.Content = "Result: " + MES.CalculateValue(getArguments())[index].ToString();
            }
        }
        private void AddSliders()
        {
            if (MES.InputsNumber % 2 == 0)
            {
                Inputs.ColumnDefinitions.Add(new ColumnDefinition());
                Inputs.ColumnDefinitions.Add(new ColumnDefinition());
                for (int i = 0; i < MES.InputsNumber / 2; i++)
                {
                    Inputs.RowDefinitions.Add(new RowDefinition());
                }
                for (int i = 0; i < MES.InputsNumber / 2; i++)
                {
                    Slider tb = new Slider();
                    TextBox t = new TextBox();
                    Label label = new Label();
                    label.Content = MES.Inputs[i * 2].Name;
                    tb.Name = MES.Inputs[i * 2].Name;
                    t.Name = MES.Inputs[i * 2].Name + "TB";
                    label.Foreground = Brushes.White;
                    t.LostFocus += Tb1_LostFocus;
                    StackPanel sp = new StackPanel();
                    StackPanel spp = new StackPanel();
                    spp.Children.Add(label);
                    spp.Children.Add(t);
                    spp.Orientation = Orientation.Horizontal;
                    sp.Children.Add(tb);
                    sp.Children.Add(spp);
                    Inputs.Children.Add(sp);
                    Grid.SetColumn(sp, 0);
                    Grid.SetRow(sp, i);
                    tb.Maximum = MES.Inputs[i * 2].UpperLimitOfRange;
                    tb.Minimum = MES.Inputs[i * 2].LowerLimitOfRange;
                    slidersList.Add(tb);
                    textBoxList.Add(t);

                    Slider tb1 = new Slider();
                    TextBox t1 = new TextBox();
                    Label label1 = new Label();
                    label1.Content = MES.Inputs[i * 2 + 1].Name;
                    tb1.Name = MES.Inputs[i * 2 + 1].Name;
                    t1.Name = MES.Inputs[i * 2 + 1].Name + "TB";
                    label1.Foreground = Brushes.White;
                    StackPanel sp1 = new StackPanel();
                    StackPanel spp1 = new StackPanel();
                    spp1.Children.Add(label1);
                    spp1.Children.Add(t1);
                    spp1.Orientation = Orientation.Horizontal;
                    sp1.Children.Add(tb1);
                    sp1.Children.Add(spp1);
                    Inputs.Children.Add(sp1);
                    Grid.SetColumn(sp1, 1);
                    Grid.SetRow(sp1, i);
                    tb1.Maximum = MES.Inputs[i * 2 + 1].UpperLimitOfRange;
                    tb1.Minimum = MES.Inputs[i * 2 + 1].LowerLimitOfRange;
                    slidersList.Add(tb1);
                    textBoxList.Add(t1);
                    t1.Width = 40;
                    t.Width = 40;
                    t.LostFocus += Tb1_LostFocus;
                    tb.ValueChanged += Tb_ValueChanged;
                    tb1.ValueChanged += Tb_ValueChanged;
                    t1.LostFocus += Tb1_LostFocus;
                }
            }

            else
            {
                Inputs.ColumnDefinitions.Add(new ColumnDefinition());
                for (int i = 0; i < MES.InputsNumber; i++)
                {
                    Inputs.RowDefinitions.Add(new RowDefinition());
                }
                int j = 0;
                foreach (LinguisticVariable Input in MES.Inputs)
                {
                    Slider tb = new Slider();
                    TextBox t = new TextBox();
                    t.LostFocus += Tb1_LostFocus;
                    tb.ValueChanged += Tb_ValueChanged;
                    Label label = new Label();
                    label.Content = Input.Name;
                    tb.Name = Input.Name;
                    t.Name = Input.Name + "TB";
                    label.Foreground = Brushes.White;
                    StackPanel sp = new StackPanel();
                    StackPanel spp = new StackPanel();
                    spp.Children.Add(label);
                    spp.Children.Add(t);
                    spp.Orientation = Orientation.Horizontal;
                    sp.Children.Add(tb);
                    sp.Children.Add(spp);
                    Inputs.Children.Add(sp);
                    Grid.SetColumn(sp, 0);
                    Grid.SetRow(sp, j);
                    tb.Maximum = MES.Inputs[j].UpperLimitOfRange;
                    tb.Minimum = MES.Inputs[j].LowerLimitOfRange;
                    slidersList.Add(tb);
                    textBoxList.Add(t);
                    t.Width = 40;
                    j++;
                }
            }
        }

        private void Tb_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                CalculateValue();
                RefreshPlot();
                var s = sender as Slider;
                double value = s.Value;
                foreach(TextBox tb in textBoxList)
                {
                    if(tb.Name.Equals(s.Name + "TB"))
                    {
                        tb.Text = value.ToString();
                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void AddTextBoxes()
        {
            if (MES.InputsNumber % 2 == 0)
            {
                Inputs.ColumnDefinitions.Add(new ColumnDefinition());
                Inputs.ColumnDefinitions.Add(new ColumnDefinition());
                for (int i = 0; i < MES.InputsNumber / 2; i++)
                {
                    Inputs.RowDefinitions.Add(new RowDefinition());
                }
                for (int i = 0; i < MES.InputsNumber / 2; i++)
                {
                    TextBox tb = new TextBox();
                    TextBox t = new TextBox();
                    Label label = new Label();
                    label.Content = MES.Inputs[i * 2].Name;
                    tb.Name = MES.Inputs[i * 2].Name;
                    label.Foreground = Brushes.White;
                    tb.LostFocus += Tb1_LostFocus;
                    StackPanel sp = new StackPanel();
                    sp.Children.Add(tb);
                    sp.Children.Add(label);
                    Inputs.Children.Add(sp);
                    Grid.SetColumn(sp, 0);
                    Grid.SetRow(sp, i);
                    textBoxList.Add(tb);


                    TextBox tb1 = new TextBox();
                    Label label1 = new Label();
                    label1.Content = MES.Inputs[i * 2 + 1].Name;
                    tb1.Name = MES.Inputs[i * 2 + 1].Name;
                    label1.Foreground = Brushes.White;
                    tb1.LostFocus += Tb1_LostFocus;
                    StackPanel sp1 = new StackPanel();
                    sp1.Children.Add(tb1);
                    sp1.Children.Add(label1);
                    Inputs.Children.Add(sp1);
                    Grid.SetColumn(sp1, 1);
                    Grid.SetRow(sp1, i);
                    textBoxList.Add(tb1);

                }
            }

            else
            {
                Inputs.ColumnDefinitions.Add(new ColumnDefinition());
                for (int i = 0; i < MES.InputsNumber; i++)
                {
                    Inputs.RowDefinitions.Add(new RowDefinition());
                }
                int j = 0;
                foreach (LinguisticVariable Input in MES.Inputs)
                {
                    TextBox tb = new TextBox();
                    tb.LostFocus += Tb1_LostFocus;
                    Label label = new Label();
                    label.Content = Input.Name;
                    tb.Name = Input.Name;
                    label.Foreground = Brushes.White;
                    StackPanel sp = new StackPanel();
                    sp.Children.Add(tb);
                    sp.Children.Add(label);
                    Inputs.Children.Add(sp);
                    Grid.SetColumn(sp, 0);
                    Grid.SetRow(sp, j);
                    textBoxList.Add(tb);
                    j++;
                }
            }
        }

        private void EnableButtons()
        {

        }
        private void Tb1_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var s = sender as TextBox;
                double value;
                Slider slider = null;
                string sliderName = s.Name.Remove(s.Name.Length - 2, 2);
                foreach (Slider sl in slidersList)
                {
                    if (sl.Name.Equals(sliderName))
                    {
                        slider = sl;
                        break;
                    }
                }
                if (double.TryParse(s.Text, out value))
                {
                    if (slider != null)
                    {
                        slider.Value = value;
                    }
                }
                else
                {
                    s.Name = 0.ToString();
                    slider.Value = 0;
                }

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void RefreshPlot()
        {
            PlotModel plotModel = new PlotModel();
            LineSeries ls = new LineSeries();
            List<double> arguments = getArguments();
            double interval = (MES.Outputs[index].UpperLimitOfRange - MES.Outputs[index].LowerLimitOfRange) / Math.Min(MES.precission, 128);
            for (double i = MES.Outputs[index].LowerLimitOfRange; i <= MES.Outputs[index].UpperLimitOfRange; i += interval)
            {
                ls.Points.Add(new DataPoint(i, MES.GetOutputFunction(arguments, i, index)));
            }
            

            plotModel.Series.Add(ls);
            LinearAxis lax = plotModel.DefaultXAxis as LinearAxis;
            LinearAxis lay = plotModel.DefaultYAxis as LinearAxis;
            
            if(lax != null && lay != null)
            {
                lax.AbsoluteMaximum = MES.Outputs[index].LowerLimitOfRange;
                lax.AbsoluteMaximum = MES.Outputs[index].UpperLimitOfRange;
                lay.AbsoluteMaximum = 1;
                lay.AbsoluteMinimum = 0;
            }
            ActualPlot.Model = plotModel;



        }
        private void AddRadioBoxes()
        {
            if (MES.OutputsNumber % 2 == 0)
            {
                Outputs.ColumnDefinitions.Add(new ColumnDefinition());
                Outputs.ColumnDefinitions.Add(new ColumnDefinition());
                for (int i = 0; i < MES.OutputsNumber / 2 + 1; i++)
                {
                    Outputs.RowDefinitions.Add(new RowDefinition());
                }
                for (int i = 0; i < MES.OutputsNumber / 2; i++)
                {
                    RadioButton rb = new RadioButton();
                    rb.Content = MES.Outputs[i * 2].Name;
                    rb.Name = MES.Outputs[i * 2].Name;
                    rb.Checked += Rb_Checked;
                    rb.Foreground = Brushes.White;
                    Grid.SetColumn(rb, 0);
                    Grid.SetRow(rb, i);
                    Outputs.Children.Add(rb);
                    radioButtonsList.Add(rb);

                    RadioButton rb1 = new RadioButton();
                    rb1.Content = MES.Outputs[i * 2 + 1].Name;
                    rb1.Name = MES.Outputs[i * 2 + 1].Name;
                    rb1.Checked += Rb_Checked;
                    rb1.Foreground = Brushes.White;
                    Grid.SetColumn(rb1, 1);
                    Grid.SetRow(rb1, i);
                    Outputs.Children.Add(rb1);
                    radioButtonsList.Add(rb1);
                }
            }
            else
            {
                Outputs.ColumnDefinitions.Add(new ColumnDefinition());
                for (int i = 0; i < MES.OutputsNumber + 1; i++)
                {
                    Outputs.RowDefinitions.Add(new RowDefinition());
                }
                int j = 0;
                foreach (LinguisticVariable output in MES.Outputs)
                {
                    RadioButton rb = new RadioButton();
                    rb.Content = output.Name;
                    rb.Name = output.Name;
                    rb.Checked += Rb_Checked;
                    rb.Foreground = Brushes.White;
                    Grid.SetColumn(rb, 0);
                    Grid.SetRow(rb, j);
                    Outputs.Children.Add(rb);
                    radioButtonsList.Add(rb);
                    j++;
                }

            }
        }

        private void Rb_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                RadioButton rb = (RadioButton)sender;
                index = MES.FindOutputByName(rb.Name);
                RefreshPlot();
                CalculateValue();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private List<double> getArguments()
        {
            List<double> arguments = new List<double>();
            foreach (Slider tb in slidersList)
            {
                try
                {
                    arguments.Add(tb.Value);
                }
                catch
                {
                    throw new ArgumentException("Wrong number format.");
                }
            }
            return arguments;
        }
    }
}
