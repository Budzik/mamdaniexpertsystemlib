﻿#pragma checksum "..\..\LinguisticVariableEditing.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8D0851C87518897B6079D9E9F7EBDB89"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MamdaniExpertSystem;
using OxyPlot.Wpf;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MamdaniExpertSystem {
    
    
    /// <summary>
    /// LinguisticVariableEditing
    /// </summary>
    public partial class LinguisticVariableEditing : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 30 "..\..\LinguisticVariableEditing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal OxyPlot.Wpf.PlotView ActualPlot;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\LinguisticVariableEditing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MembershipFunctionName;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\LinguisticVariableEditing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Parameters;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\LinguisticVariableEditing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox TypeOfFunction;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\LinguisticVariableEditing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox MembershipFunctionList;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\LinguisticVariableEditing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LowerLimit;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\LinguisticVariableEditing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox UpperLimit;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MamdaniExpertSystem;component/linguisticvariableediting.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\LinguisticVariableEditing.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ActualPlot = ((OxyPlot.Wpf.PlotView)(target));
            return;
            case 2:
            this.MembershipFunctionName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.Parameters = ((System.Windows.Controls.TextBox)(target));
            
            #line 44 "..\..\LinguisticVariableEditing.xaml"
            this.Parameters.GotFocus += new System.Windows.RoutedEventHandler(this.Parameters_Focus);
            
            #line default
            #line hidden
            return;
            case 4:
            this.TypeOfFunction = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.MembershipFunctionList = ((System.Windows.Controls.ListBox)(target));
            
            #line 53 "..\..\LinguisticVariableEditing.xaml"
            this.MembershipFunctionList.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ChangeSelection);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 54 "..\..\LinguisticVariableEditing.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.AddMembershipFunction);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 60 "..\..\LinguisticVariableEditing.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.DeleteMembershipFunction);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 66 "..\..\LinguisticVariableEditing.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.EditMembershipFunction);
            
            #line default
            #line hidden
            return;
            case 9:
            this.LowerLimit = ((System.Windows.Controls.TextBox)(target));
            
            #line 80 "..\..\LinguisticVariableEditing.xaml"
            this.LowerLimit.LostFocus += new System.Windows.RoutedEventHandler(this.LowerLimitChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.UpperLimit = ((System.Windows.Controls.TextBox)(target));
            
            #line 84 "..\..\LinguisticVariableEditing.xaml"
            this.UpperLimit.LostFocus += new System.Windows.RoutedEventHandler(this.UpperLimitChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

