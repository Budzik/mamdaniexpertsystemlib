﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Test.LinguisticVariables
{
    [TestFixture]
    class LinguisticVariableTest
    {
        [Test]
        public void AddMembershipFunction_NewTriangleMembershipFunction_NumberOfMembershipFunctionIs1()
        {
            MembershipFunction triangleFunction = new Triangle();
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            linguisticVariable.AddMembershipFunction(triangleFunction);
            Assert.That(linguisticVariable.NumberOfMembershipFunctions, Is.EqualTo(1));
        }
        [Test]
        public void AddMembershipFunction_NewTwoMembershipFunction_NumberOfMembershipFunctionIs2()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            linguisticVariable.AddMembershipFunction(new Singleton());
            linguisticVariable.AddMembershipFunction(new Constant());
            Assert.That(linguisticVariable.NumberOfMembershipFunctions, Is.EqualTo(2));
        }
        [Test]
        public void AddMembershipFunction_NewTwoMembershipFunction_SecondElementIsCorrect()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Singleton singleton = new Singleton();
            singleton.Parameters.Add(1);
            singleton.Name = "bla";
            Constant constant = new Constant();
            constant.Parameters.Add(1);
            constant.Name = "bula";
            linguisticVariable.AddMembershipFunction(singleton);
            linguisticVariable.AddMembershipFunction(constant);
            string text = linguisticVariable.GetMembershipFunctions()[1];
            Assert.That(text, Is.EqualTo("Constant function bula [1]"));
        }
        [Test]
        public void RemoveMembershipFunction_ArgumentIs1_NumberOfMembershipFunctionIs2()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            linguisticVariable.AddMembershipFunction(new Singleton());
            linguisticVariable.AddMembershipFunction(new Constant());
            linguisticVariable.AddMembershipFunction(new Triangle());
            linguisticVariable.RemoveMembershipFunction(1);
            Assert.That(linguisticVariable.NumberOfMembershipFunctions, Is.EqualTo(2));
        }
        [Test]
        public void EditMembershipFunction_1AndTriangleMembershipFunction_SecondElementIsCorrect()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Singleton singleton = new Singleton();
            singleton.Parameters.Add(1);
            singleton.Name = "bla";
            Constant constant = new Constant();
            constant.Parameters.Add(1);
            constant.Name = "bula";
            Triangle triangle = new Triangle();
            triangle.Name = "triangle";
            triangle.Parameters.Add(1);
            triangle.Parameters.Add(2);
            triangle.Parameters.Add(3);
            linguisticVariable.AddMembershipFunction(singleton);
            linguisticVariable.AddMembershipFunction(constant);
            linguisticVariable.EditMembershipFunction(1, triangle);
            string text = linguisticVariable.GetMembershipFunctions()[1];
            Assert.That(text, Is.EqualTo("Triangle function triangle [1 2 3]"));
        }
        [Test]
        public void FindMembershipFunction_ExistingName_ElementIsCorrect()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Singleton singleton = new Singleton();
            singleton.Parameters.Add(1);
            singleton.Name = "bla";
            Constant constant = new Constant();
            constant.Parameters.Add(1);
            constant.Name = "bula";
            Triangle triangle = new Triangle();
            triangle.Name = "triangle";
            triangle.Parameters.Add(1);
            triangle.Parameters.Add(2);
            triangle.Parameters.Add(3);
            linguisticVariable.AddMembershipFunction(singleton);
            linguisticVariable.AddMembershipFunction(constant);
            linguisticVariable.EditMembershipFunction(1, triangle);
            string text = linguisticVariable.FindMembershipFunction("triangle").DescribeMembershipFunction();
            Assert.That(text, Is.EqualTo("Triangle function triangle [1 2 3]"));
        }
        [Test]
        public void FindMembershipFunction_NotExistingName_ArgumentException()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Singleton singleton = new Singleton();
            singleton.Parameters.Add(1);
            singleton.Name = "bla";
            Constant constant = new Constant();
            constant.Parameters.Add(1);
            constant.Name = "bula";
            linguisticVariable.AddMembershipFunction(singleton);
            linguisticVariable.AddMembershipFunction(constant);
            Assert.Throws(typeof(ArgumentException), ()=>linguisticVariable.FindMembershipFunction("blabla"));
        }
        [Test]
        public void DesignateMembershipGrades_NumberInRange_3ElementsCollection()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Triangle triangle1 = new Triangle();
            triangle1.Name = "triangle1";
            triangle1.Parameters = new List<Double>() { 1, 2, 3 };
            Triangle triangle2 = new Triangle();
            triangle2.Name = "triangle2";
            triangle2.Parameters = new List<Double>() { 2, 3, 4 };
            Triangle triangle3 = new Triangle();
            triangle3.Name = "triangle3";
            triangle3.Parameters = new List<Double>() { 3, 4, 5 };
            linguisticVariable.LowerLimitOfRange = 1;
            linguisticVariable.UpperLimitOfRange = 5;
            linguisticVariable.AddMembershipFunction(triangle1);
            linguisticVariable.AddMembershipFunction(triangle2);
            linguisticVariable.AddMembershipFunction(triangle3);
            int numberOfElements = linguisticVariable.DesignateMembershipGrades(3).Count;
            Assert.That(numberOfElements, Is.EqualTo(3));
        }
        [Test]
        public void DesignateMembershipGrades_ArgumentIs3_SecondElementIs1()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Triangle triangle1 = new Triangle();
            triangle1.Name = "triangle1";
            triangle1.Parameters = new List<Double>() { 1, 2, 3 };
            Triangle triangle2 = new Triangle();
            triangle2.Name = "triangle2";
            triangle2.Parameters = new List<Double>() { 2, 3, 4 };
            Triangle triangle3 = new Triangle();
            triangle3.Name = "triangle3";
            triangle3.Parameters = new List<Double>() { 3, 4, 5 };
            linguisticVariable.LowerLimitOfRange = 1;
            linguisticVariable.UpperLimitOfRange = 5;
            linguisticVariable.AddMembershipFunction(triangle1);
            linguisticVariable.AddMembershipFunction(triangle2);
            linguisticVariable.AddMembershipFunction(triangle3);
            double numberOfElements = linguisticVariable.DesignateMembershipGrades(3)["triangle2"];
            Assert.That(numberOfElements, Is.EqualTo(1));
        }
        [Test]
        public void DesignateMembershipGrades_ArgumentIsEqualToLowerLimit_NotThrowException()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Triangle triangle1 = new Triangle();
            triangle1.Name = "triangle1";
            triangle1.Parameters = new List<Double>() { 1, 2, 3 };
            Triangle triangle2 = new Triangle();
            triangle2.Name = "triangle2";
            triangle2.Parameters = new List<Double>() { 2, 3, 4 };
            Triangle triangle3 = new Triangle();
            triangle3.Name = "triangle3";
            triangle3.Parameters = new List<Double>() { 3, 4, 5 };
            linguisticVariable.LowerLimitOfRange = 1;
            linguisticVariable.UpperLimitOfRange = 5;
            linguisticVariable.AddMembershipFunction(triangle1);
            linguisticVariable.AddMembershipFunction(triangle2);
            linguisticVariable.AddMembershipFunction(triangle3);
            Assert.DoesNotThrow(() =>  linguisticVariable.DesignateMembershipGrades(1));
        }
        [Test]
        public void DesignateMembershipGrades_ArgumentIsEqualToUpperLimit_NotThrowException()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Triangle triangle1 = new Triangle();
            triangle1.Name = "triangle1";
            triangle1.Parameters = new List<Double>() { 1, 2, 3 };
            Triangle triangle2 = new Triangle();
            triangle2.Name = "triangle2";
            triangle2.Parameters = new List<Double>() { 2, 3, 4 };
            Triangle triangle3 = new Triangle();
            triangle3.Name = "triangle3";
            triangle3.Parameters = new List<Double>() { 3, 4, 5 };
            linguisticVariable.LowerLimitOfRange = 1;
            linguisticVariable.UpperLimitOfRange = 5;
            linguisticVariable.AddMembershipFunction(triangle1);
            linguisticVariable.AddMembershipFunction(triangle2);
            linguisticVariable.AddMembershipFunction(triangle3);
            Assert.DoesNotThrow(() => linguisticVariable.DesignateMembershipGrades(5));
        }
        [Test]
        public void DesignateMembershipGrades_ArgumentIsHigherThenUpperLimit_ThrowArgumentException()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Triangle triangle1 = new Triangle();
            triangle1.Name = "triangle1";
            triangle1.Parameters = new List<Double>() { 1, 2, 3 };
            Triangle triangle2 = new Triangle();
            triangle2.Name = "triangle2";
            triangle2.Parameters = new List<Double>() { 2, 3, 4 };
            Triangle triangle3 = new Triangle();
            triangle3.Name = "triangle3";
            triangle3.Parameters = new List<Double>() { 3, 4, 5 };
            linguisticVariable.LowerLimitOfRange = 1;
            linguisticVariable.UpperLimitOfRange = 5;
            linguisticVariable.AddMembershipFunction(triangle1);
            linguisticVariable.AddMembershipFunction(triangle2);
            linguisticVariable.AddMembershipFunction(triangle3);
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => linguisticVariable.DesignateMembershipGrades(8));
        }
        [Test]
        public void DesignateMembershipGrades_ArgumentIsLessThenLowerLimit_ThrowArgumentException()
        {
            LinguisticVariable linguisticVariable = new LinguisticVariable();
            Triangle triangle1 = new Triangle();
            triangle1.Name = "triangle1";
            triangle1.Parameters = new List<Double>() { 1, 2, 3 };
            Triangle triangle2 = new Triangle();
            triangle2.Name = "triangle2";
            triangle2.Parameters = new List<Double>() { 2, 3, 4 };
            Triangle triangle3 = new Triangle();
            triangle3.Name = "triangle3";
            triangle3.Parameters = new List<Double>() { 3, 4, 5 };
            linguisticVariable.LowerLimitOfRange = 1;
            linguisticVariable.UpperLimitOfRange = 5;
            linguisticVariable.AddMembershipFunction(triangle1);
            linguisticVariable.AddMembershipFunction(triangle2);
            linguisticVariable.AddMembershipFunction(triangle3);
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => linguisticVariable.DesignateMembershipGrades(-4));
        }
        [Test]
        public void AddFunctions_TwoFunctionWithTheSameName_ArgumentException()
        {
            LinguisticVariable lv = new LinguisticVariable();
            Triangle tr = new Triangle();
            tr.Name = "a";
            Triangle tr1 = new Triangle();
            tr1.Name = "a";
            lv.AddMembershipFunction(tr);
            Assert.Throws(typeof(ArgumentException), () => lv.AddMembershipFunction(tr1));
        }
        [Test]
        public void Name_UpperCharacters_ConvertToLower()
        {
            LinguisticVariable lv = new LinguisticVariable();
            lv.Name = "AAA";
            string result = lv.Name;
            Assert.That(result, Is.EqualTo("aaa"));
        }
    }
}
