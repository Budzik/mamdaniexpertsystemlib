﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Test.MembershipFunctions
{
    class ConstantTest
    {
        [Test]
        public void NumberOfArguments_GetNumberOfArgument_1()
        {
            IMembershipFunction constant = new Constant() { Parameters = new List<double>() { 1 } };
            int numberOfArguments = constant.NumberOfArguments;
            Assert.That(numberOfArguments, Is.EqualTo(1));
        }
        [Test]
        public void NumberOfArguments_GetIncreasingFlag_True()
        {
            IMembershipFunction constant = new Constant() { Parameters = new List<double>() { 1 } };
            bool increasingFlag = constant.IncreasingFlag;
            Assert.That(increasingFlag, Is.EqualTo(false));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1Parameters1_1()
        {
            IMembershipFunction constant = new Constant() { Parameters = new List<double>() { 1 } };
            double value = constant.DesignateMembershipGradeOfFuzzySet(1);
            Assert.That(value, Is.EqualTo(1));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument999Parameters0point33_0point33()
        {
            IMembershipFunction constant = new Constant() { Parameters = new List<double>() { 0.33 } };
            double value = constant.DesignateMembershipGradeOfFuzzySet(999);
            Assert.That(value, Is.EqualTo(0.33));
        }
        [Test]
        public void SetParameters_ParametersMinus1_ArgumentException()
        {
            Constant constant = new Constant();
            Assert.Throws(typeof(ArgumentException), () => { constant.Parameters = new List<double>() { -1 }; });
        }
        [Test]
        public void SetParameters_Parameters2_FormatException()
        {
            MembershipFunction membershipFunction = new Constant();
            Assert.Throws(typeof(ArgumentException), () => { membershipFunction.Parameters = new List<double>() { 2 }; });
        }


    }
}
