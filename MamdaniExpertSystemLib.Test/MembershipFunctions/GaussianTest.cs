﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using NUnit.Framework;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Test.MembershipFunctions
{
    [TestFixture]
    class GaussianTest
    {
        [Test]
        public void NumberOfArguments_GetNumberOfArgument_2()
        {
            IMembershipFunction gaussian = new Gaussian() { Parameters = new List<double>() { 1, 2 } };
            int numberOfArguments = gaussian.NumberOfArguments;
            Assert.That(numberOfArguments, Is.EqualTo(2));
        }
        [Test]
        public void NumberOfArguments_GetIncreasingFlag_False()
        {
            IMembershipFunction gaussian = new Gaussian() { Parameters = new List<double>() { 1, 2 } };
            bool increasingFlag = gaussian.IncreasingFlag;
            Assert.That(increasingFlag, Is.EqualTo(false));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1Parameters12_0()
        {
            IMembershipFunction gaussian = new Gaussian() { Parameters = new List<double>() { 1, 2 } };
            double value = gaussian.DesignateMembershipGradeOfFuzzySet(1.0);
            Assert.That(value, Is.EqualTo(1));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument2Parameters12_1()
        {
            IMembershipFunction gaussian = new Gaussian() { Parameters = new List<double>() { 1, 2 } };
            double value = gaussian.DesignateMembershipGradeOfFuzzySet(2.0);
            Assert.AreEqual(0.8824969, value, 0.001);
        }

        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument13Parameters43_1()
        {
            IMembershipFunction gaussian = new Gaussian() { Parameters = new List<double>() { 4, 3 } };
            double value = gaussian.DesignateMembershipGradeOfFuzzySet(13.0);
            Assert.AreEqual(0.011108, value, 0.001);
        }
    }
}
