﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Test.MembershipFunctions
{
    [TestFixture]
    class MembershipFunctionGenerally
    {
        [Test]
        public void SetParameters_GoodParameters_NoException()
        {
            MembershipFunction membershipFunction = new Triangle();
            Assert.DoesNotThrow(() => { membershipFunction.Parameters = new List<double>() { 1, 2, 3 }; });
        }
        [Test]
        public void SetParameters_TooLittleParameters_FormatException()
        {
            MembershipFunction membershipFunction = new Triangle();
            Assert.Throws(typeof(ArgumentException),() => { membershipFunction.Parameters = new List<double>() { 1, 2 }; });
        }
        [Test]
        public void SetParameters_TooMuchParameters_FormatException()
        {
            MembershipFunction membershipFunction = new Triangle();
            Assert.Throws(typeof(ArgumentException), () => { membershipFunction.Parameters = new List<double>() { 1, 2, 3, 4 }; });
        }
        [Test]
        public void SetParameters_NotAscendingOrderParametersWithIncreasingFlag_FormatException()
        {
            MembershipFunction membershipFunction = new Triangle();
            Assert.Throws(typeof(ArgumentException), () => { membershipFunction.Parameters = new List<double>() { 3, 2, 1 }; });
        }
        [Test]
        public void SetParameters_NotAscendingOrderParametersWithoutIncreasingFlag_NoException()
        {
            MembershipFunction membershipFunction = new Gaussian();
            Assert.DoesNotThrow(() => { membershipFunction.Parameters = new List<double>() { 3, 2 }; });
        }
        [Test]
        public void Name_UpperCharacters_ConvertToLower()
        {
            MembershipFunction lv = new Triangle();
            lv.Name = "AAA";
            string result = lv.Name;
            Assert.That(result, Is.EqualTo("aaa"));
        }

    }
}
