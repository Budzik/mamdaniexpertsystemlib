﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using NUnit.Framework;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Test.MembershipFunctions
{
    [TestFixture]
    class SingletonTest
    {
        [Test]
        public void NumberOfArguments_GetNumberOfArgument_1()
        {
            IMembershipFunction singleton = new Singleton() { Parameters = new List<double>() { 1 } };
            int numberOfArguments = singleton.NumberOfArguments;
            Assert.That(numberOfArguments, Is.EqualTo(1));
        }
        [Test]
        public void NumberOfArguments_GetIncreasingFlag_False()
        {
            IMembershipFunction singleton = new Singleton() { Parameters = new List<double>() { 1 } };
            bool increasingFlag = singleton.IncreasingFlag;
            Assert.That(increasingFlag, Is.EqualTo(false));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument0Parameters1_0()
        {
            IMembershipFunction singleton = new Singleton() { Parameters = new List<double>() { 1 } };
            double value = singleton.DesignateMembershipGradeOfFuzzySet(0);
            Assert.That(value, Is.EqualTo(0));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1Parameters1_1()
        {
            IMembershipFunction singleton = new Singleton() { Parameters = new List<double>() { 1 } };
            double value = singleton.DesignateMembershipGradeOfFuzzySet(1);
            Assert.That(value, Is.EqualTo(1));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument2Parameters1_0()
        {
            IMembershipFunction singleton = new Singleton() { Parameters = new List<double>() { 1 } };
            double value = singleton.DesignateMembershipGradeOfFuzzySet(2);
            Assert.That(value, Is.EqualTo(0));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument2Parameters2_1()
        {
            IMembershipFunction singleton = new Singleton() { Parameters = new List<double>() { 2 } };
            double value = singleton.DesignateMembershipGradeOfFuzzySet(2);
            Assert.That(value, Is.EqualTo(1));
        }
    }
}
