﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using NUnit.Framework;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Test.MembershipFunctions
{
    [TestFixture]
    class TrapeziumTest
    {
        [Test]
        public void NumberOfArguments_GetNumberOfArgument_3()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            int numberOfArguments = trapezium.NumberOfArguments;
            Assert.That(numberOfArguments, Is.EqualTo(4));
        }
        [Test]
        public void NumberOfArguments_GetIncreasingFlag_True()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            bool increasingFlag = trapezium.IncreasingFlag;
            Assert.That(increasingFlag, Is.EqualTo(true));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument0Parameters1234_0()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(0);
            Assert.That(value, Is.EqualTo(0));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1Parameters1234_0()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(1);
            Assert.That(value, Is.EqualTo(0));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument2Parameters1234_1()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(2);
            Assert.That(value, Is.EqualTo(1));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument3Parameters1234_1()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(3);
            Assert.That(value, Is.EqualTo(1));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument4Parameters1234_0()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(4);
            Assert.That(value, Is.EqualTo(0));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1point5Parameters1234_0point5()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(1.5);
            Assert.That(value, Is.EqualTo(0.5));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument2point5Parameters1234_1()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(2.5);
            Assert.That(value, Is.EqualTo(1));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument3point5Parameters1234_0point5()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(3.5);
            Assert.That(value, Is.EqualTo(0.5));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1point6Parameters1234_0point6()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(1.6);
            Assert.AreEqual(0.6, value, 0.001);
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument3point6Parameters1234_0point4()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 1, 2, 3, 4 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(3.6);
            Assert.AreEqual(0.4, value, 0.001);
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1Parameters0111_1()
        {
            IMembershipFunction trapezium = new Trapezium() { Parameters = new List<double>() { 0, 1, 1, 1 } };
            double value = trapezium.DesignateMembershipGradeOfFuzzySet(1);
            Assert.AreEqual(1, value, 0.001);
        }
    }
}
