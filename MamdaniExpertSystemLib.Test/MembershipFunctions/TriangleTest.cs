﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using NUnit.Framework;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Test.MembershipFunctions
{
    [TestFixture]
    class TriangleTest
    {
        [Test]
        public void NumberOfArguments_GetNumberOfArgument_3()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            int numberOfArguments = triangle.NumberOfArguments;
            Assert.That(numberOfArguments, Is.EqualTo(3));
        }
        [Test]
        public void NumberOfArguments_GetIncreasingFlag_True()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            bool increasingFlag = triangle.IncreasingFlag;
            Assert.That(increasingFlag, Is.EqualTo(true));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument0Parameters123_0()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(0.0);
            Assert.That(value, Is.EqualTo(0));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument2Parameters123_1()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(2.0);
            Assert.That(value, Is.EqualTo(1));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1point5Parameters123_0point5()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(1.5);
            Assert.That(value, Is.EqualTo(0.5));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument2point5Parameters123_0point5()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(2.5);
            Assert.That(value, Is.EqualTo(0.5));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1point5Parameters122_0point5()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 2 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(1.5);
            Assert.That(value, Is.EqualTo(0.5));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1point6Parameters123_0point5()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(1.6);
            Assert.AreEqual(0.6, value, 0.001);
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument2point6Parameters123_0point5()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(2.6);
            Assert.AreEqual(0.4, value, 0.001);
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1Parameters123_0()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(1);
            Assert.That(value, Is.EqualTo(0));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument3Parameters123_0()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 1, 2, 3 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(3);
            Assert.That(value, Is.EqualTo(0));
        }
        [Test]
        public void DesignateMembershipGradeOfFuzzySet_Argument1Parameters011_1()
        {
            IMembershipFunction triangle = new Triangle() { Parameters = new List<double>() { 0, 1, 1 } };
            double value = triangle.DesignateMembershipGradeOfFuzzySet(1);
            Assert.That(value, Is.EqualTo(1));
        }
    }
}
