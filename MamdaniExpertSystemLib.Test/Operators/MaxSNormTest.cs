﻿using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.Test.Operators
{
    [TestFixture]
    class MaxSNormTest
    {
        [Test]
        public void DesignateValue_OneArgumentIsWrong_ArgumentException()
        {
            TwoArgumentsOperator maxSNorm = new MaxSNorm();
            Assert.Throws(typeof(ArgumentException), () => maxSNorm.DesignateValue(2, 0.33));
        }
        [Test]
        public void DesignateValue_TwoArgumentsIsWrong_ArgumentException()
        {
            TwoArgumentsOperator maxSNorm = new MaxSNorm();
            Assert.Throws(typeof(ArgumentException), () => maxSNorm.DesignateValue(3, -2));
        }
        [Test]
        public void DesignateValue_ArgumentsIs0And1_1()
        {
            TwoArgumentsOperator maxSNorm = new MaxSNorm();
            double value = maxSNorm.DesignateValue(0, 1);
            Assert.AreEqual(1, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs1And1_1()
        {
            TwoArgumentsOperator maxSNorm = new MaxSNorm();
            double value = maxSNorm.DesignateValue(1, 1);
            Assert.AreEqual(1, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs1And0_1()
        {
            TwoArgumentsOperator maxSNorm = new MaxSNorm();
            double value = maxSNorm.DesignateValue(1, 0);
            Assert.AreEqual(1, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0And0_0()
        {
            TwoArgumentsOperator maxSNorm = new MaxSNorm();
            double value = maxSNorm.DesignateValue(0, 0);
            Assert.AreEqual(0, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0point48And0point22_0point48()
        {
            TwoArgumentsOperator maxSNorm = new MaxSNorm();
            double value = maxSNorm.DesignateValue(0.48, 0.22);
            Assert.AreEqual(0.48, value, 0.001);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0point65And0point97_0point97()
        {
            TwoArgumentsOperator maxSNorm = new MaxSNorm();
            double value = maxSNorm.DesignateValue(0.65, 0.97);
            Assert.AreEqual(0.97, value, 0.001);
        }
    }
}
