﻿using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.TNormOperators;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.Test.Operators
{
    [TestFixture]
    class MulTNormTest
    {
        [Test]
        public void DesignateValue_OneArgumentIsWrong_ArgumentException()
        {
            TwoArgumentsOperator mulTNorm = new MulTNorm();
            Assert.Throws(typeof(ArgumentException), () => mulTNorm.DesignateValue(2, 0.33));
        }
        [Test]
        public void DesignateValue_TwoArgumentsIsWrong_ArgumentException()
        {
            TwoArgumentsOperator mulTNorm = new MulTNorm();
            Assert.Throws(typeof(ArgumentException), () => mulTNorm.DesignateValue(3, -2));
        }
        [Test]
        public void DesignateValue_ArgumentsIs0And1_0()
        {
            TwoArgumentsOperator mulTNorm = new MulTNorm();
            double value = mulTNorm.DesignateValue(0, 1);
            Assert.AreEqual(0, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs1And1_1()
        {
            TwoArgumentsOperator mulTNorm = new MulTNorm();
            double value = mulTNorm.DesignateValue(1, 1);
            Assert.AreEqual(1, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs1And0_1()
        {
            TwoArgumentsOperator mulTNorm = new MulTNorm();
            double value = mulTNorm.DesignateValue(1, 0);
            Assert.AreEqual(0, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0And0_0()
        {
            TwoArgumentsOperator mulTNorm = new MulTNorm();
            double value = mulTNorm.DesignateValue(0, 0);
            Assert.AreEqual(0, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0point48And0point22_0point1056()
        {
            TwoArgumentsOperator mulTNorm = new MulTNorm();
            double value = mulTNorm.DesignateValue(0.48, 0.22);
            Assert.AreEqual(0.1056, value, 0.001);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0point65And0point97_0point6305()
        {
            TwoArgumentsOperator mulTNorm = new MulTNorm();
            double value = mulTNorm.DesignateValue(0.65, 0.97);
            Assert.AreEqual(0.6305, value, 0.001);
        }
    }
}
