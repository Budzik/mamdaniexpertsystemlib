﻿using MamdaniExpertSystemLib.Operators.OneArgumentOperators.Concrete.Negation;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.Test.Operators
{
    [TestFixture]
    class NormalNegationTest
    {
        [Test]
        public void DesignateValue_ArgumentIsTooBig_ArgumentException()
        {
            NormalNegation normalNegation = new NormalNegation();
            Assert.Throws(typeof(ArgumentException), () => normalNegation.DesignateValue(2));
        }
        [Test]
        public void DesignateValue_ArgumentIsTooLow_ArgumentException()
        {
            NormalNegation normalNegation = new NormalNegation();
            Assert.Throws(typeof(ArgumentException), () => normalNegation.DesignateValue(-1));
        }
        [Test]
        public void DesignateValue_ArgumentIs1_0()
        {
            NormalNegation normalNegation = new NormalNegation();
            double value = normalNegation.DesignateValue(0);
            Assert.That(value, Is.EqualTo(1));
        }
        [Test]
        public void DesignateValue_ArgumentIs0point33_0point67()
        {
            NormalNegation normalNegation = new NormalNegation();
            double value = normalNegation.DesignateValue(0.33);
            Assert.AreEqual(0.67, value, 0.001);
        }
        [Test]
        public void DesignateValue_DoubleNegation_ResultTheSameLikeArgument()
        {
            NormalNegation normalNegation = new NormalNegation();
            double value = normalNegation.DesignateValue(normalNegation.DesignateValue(0.33));
            Assert.AreEqual(0.33, value, 0.001);
        }
    }
}
