﻿using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.Test.Operators
{
    [TestFixture]
    class SumSNormTest
    {
        [Test]
        public void DesignateValue_OneArgumentIsWrong_ArgumentException()
        {
            TwoArgumentsOperator sumSNorm = new SumSNorm();
            Assert.Throws(typeof(ArgumentException), () => sumSNorm.DesignateValue(2, 0.33));
        }
        [Test]
        public void DesignateValue_TwoArgumentsIsWrong_ArgumentException()
        {
            TwoArgumentsOperator sumSNorm = new SumSNorm();
            Assert.Throws(typeof(ArgumentException), () => sumSNorm.DesignateValue(3, -2));
        }
        [Test]
        public void DesignateValue_ArgumentsIs0And1_1()
        {
            TwoArgumentsOperator sumSNorm = new SumSNorm();
            double value = sumSNorm.DesignateValue(0, 1);
            Assert.AreEqual(1, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs1And1_1()
        {
            TwoArgumentsOperator sumSNorm = new SumSNorm();
            double value = sumSNorm.DesignateValue(1, 1);
            Assert.AreEqual(1, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs1And0_1()
        {
            TwoArgumentsOperator sumSNorm = new SumSNorm();
            double value = sumSNorm.DesignateValue(1, 0);
            Assert.AreEqual(1, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0And0_0()
        {
            TwoArgumentsOperator sumSNorm = new SumSNorm();
            double value = sumSNorm.DesignateValue(0, 0);
            Assert.AreEqual(0, value);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0point48And0point22_0point7()
        {
            TwoArgumentsOperator sumSNorm = new SumSNorm();
            double value = sumSNorm.DesignateValue(0.48, 0.22);
            Assert.AreEqual(0.7, value, 0.001);
        }
        [Test]
        public void DesignateValue_ArgumentsIs0point65And0point97_1()
        {
            TwoArgumentsOperator sumSNorm = new SumSNorm();
            double value = sumSNorm.DesignateValue(0.65, 0.97);
            Assert.AreEqual(1, value, 0.001);
        }
    }
}
