﻿using MamdaniExpertSystemLib.Rules;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.Test.Rules
{
    [TestFixture]
    class ElementOfExpressionTest
    {
        [Test]
        public void ToString_SNorm_Plus()
        {
            ElementOfExpression elementOfExpression = new ElementOfExpression();
            elementOfExpression.TypeOfElement = TypeOfElement.SNorm;
            string text = elementOfExpression.ToString();
            Assert.That(text, Is.EqualTo("+"));
        }
        [Test]
        public void ToString_TNorm_Star()
        {
            ElementOfExpression elementOfExpression = new ElementOfExpression();
            elementOfExpression.TypeOfElement = TypeOfElement.TNorm;
            string text = elementOfExpression.ToString();
            Assert.That(text, Is.EqualTo("*"));
        }
        [Test]
        public void ToString_OpeningBracket_OpeningBracket()
        {
            ElementOfExpression elementOfExpression = new ElementOfExpression();
            elementOfExpression.TypeOfElement = TypeOfElement.OpeningBracket;
            string text = elementOfExpression.ToString();
            Assert.That(text, Is.EqualTo("("));
        }
        [Test]
        public void ToString_ClosingBracket_ClosingBracket()
        {
            ElementOfExpression elementOfExpression = new ElementOfExpression();
            elementOfExpression.TypeOfElement = TypeOfElement.ClosingBracket;
            string text = elementOfExpression.ToString();
            Assert.That(text, Is.EqualTo(")"));
        }
        [Test]
        public void ToString_InputVariableWith1and1Parameters_iv1a1()
        {
            ElementOfExpression elementOfExpression = new ElementOfExpression();
            elementOfExpression.TypeOfElement = TypeOfElement.InputVariable;
            elementOfExpression.Parameters = new List<int>() { 1, 1 };
            string text = elementOfExpression.ToString();
            Assert.That(text, Is.EqualTo("iv1a1"));
        }
        [Test]
        public void ToString_OutputVariableWith1and1Parameters_ov1a1()
        {
            ElementOfExpression elementOfExpression = new ElementOfExpression();
            elementOfExpression.TypeOfElement = TypeOfElement.OutputVariable;
            elementOfExpression.Parameters = new List<int>() { 1, 1 };
            string text = elementOfExpression.ToString();
            Assert.That(text, Is.EqualTo("ov1a1"));
        }
        [Test]
        public void ToString_ConstantWithParameter1_c1()
        {
            ElementOfExpression elementOfExpression = new ElementOfExpression();
            elementOfExpression.TypeOfElement = TypeOfElement.Constant;
            elementOfExpression.Value = 0.1;
            string text = elementOfExpression.ToString();
            Assert.That(text, Is.EqualTo("0.1"));
        }
    }
}
