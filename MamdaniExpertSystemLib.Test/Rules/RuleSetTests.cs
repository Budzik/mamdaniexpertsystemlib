﻿using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using MamdaniExpertSystemLib.Rules;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.Test.Rules
{
    [TestFixture]
    class RuleSetTests
    {
        [Test]
        public void AggregateRules_TwoArguments_NumberOfResultInFirstRowIs2()
        {
            RuleSet ruleSet = new RuleSet(new List<LinguisticVariable>(), new List<LinguisticVariable>());
            MembershipFunction f1 = new Triangle(new List<double>() { 0, 0, 1 }, "nie");
            MembershipFunction f2 = new Triangle(new List<double>() { 0, 1, 1 }, "tak");
            LinguisticVariable lv1 = new LinguisticVariable();
            lv1.Name = "a";
            lv1.MembershipFunctions.Add(f1);
            lv1.MembershipFunctions.Add(f2);
            LinguisticVariable lv2 = new LinguisticVariable(lv1, "b");
            LinguisticVariable lv3 = new LinguisticVariable(lv1, "c");
            LinguisticVariable lv4 = new LinguisticVariable(lv1, "d");
            ruleSet.Outputs.Add(lv3);
            ruleSet.Outputs.Add(lv4);
            ruleSet.Inputs.Add(lv1);
            ruleSet.Inputs.Add(lv2);


            string rule1 = "if(a = tak and b = nie)then(c = tak, d = nie)";
            string rule2 = "if(0.5)then(c = tak)";
            string rule3 = "if(0.2)then(d = tak)";
            string rule4 = "if(a = nie and ( not b = nie or 0.5))then(c = nie, d = nie)";

            ruleSet.AddRule(rule1);
            ruleSet.AddRule(rule2);
            ruleSet.AddRule(rule3);
            ruleSet.AddRule(rule4);
            List<List<double>> result = ruleSet.AggragateRules(new List<double> { 0.2, 0.4 });
            Assert.That(result[0].Count, Is.EqualTo(2));
        }
        [Test]
        public void AggregateRules_TwoArguments_NumberOfResultInSecondRowIs2()
        {
            RuleSet ruleSet = new RuleSet(new List<LinguisticVariable>(), new List<LinguisticVariable>());
            MembershipFunction f1 = new Triangle(new List<double>() { 0, 0, 1 }, "nie");
            MembershipFunction f2 = new Triangle(new List<double>() { 0, 1, 1 }, "tak");
            LinguisticVariable lv1 = new LinguisticVariable();
            lv1.Name = "a";
            lv1.MembershipFunctions.Add(f1);
            lv1.MembershipFunctions.Add(f2);
            LinguisticVariable lv2 = new LinguisticVariable(lv1, "b");
            LinguisticVariable lv3 = new LinguisticVariable(lv1, "c");
            LinguisticVariable lv4 = new LinguisticVariable(lv1, "d");
            ruleSet.Outputs.Add(lv3);
            ruleSet.Outputs.Add(lv4);
            ruleSet.Inputs.Add(lv1);
            ruleSet.Inputs.Add(lv2);


            string rule1 = "if(a = tak and b = nie)then(c = tak, d = nie)";
            string rule2 = "if(0.5)then(c = tak)";
            string rule3 = "if(0.2)then(d = tak)";
            string rule4 = "if(a = nie and ( not b = nie or 0.5))then(c = nie, d = nie)";

            ruleSet.AddRule(rule1);
            ruleSet.AddRule(rule2);
            ruleSet.AddRule(rule3);
            ruleSet.AddRule(rule4);
            List<List<double>> result = ruleSet.AggragateRules(new List<double> { 0.2, 0.4 });
            Assert.That(result[1].Count, Is.EqualTo(2));
        }
        [Test]
        public void AggregateRules_TwoArguments_1and1ResultIs0point2()
        {
            RuleSet ruleSet = new RuleSet(new List<LinguisticVariable>(), new List<LinguisticVariable>());
            MembershipFunction f1 = new Triangle(new List<double>() { 0, 0, 1 }, "nie");
            MembershipFunction f2 = new Triangle(new List<double>() { 0, 1, 1 }, "tak");
            LinguisticVariable lv1 = new LinguisticVariable();
            lv1.Name = "a";
            lv1.MembershipFunctions.Add(f1);
            lv1.MembershipFunctions.Add(f2);
            LinguisticVariable lv2 = new LinguisticVariable(lv1, "b");
            LinguisticVariable lv3 = new LinguisticVariable(lv1, "c");
            LinguisticVariable lv4 = new LinguisticVariable(lv1, "d");
            ruleSet.Outputs.Add(lv3);
            ruleSet.Outputs.Add(lv4);
            ruleSet.Inputs.Add(lv1);
            ruleSet.Inputs.Add(lv2);
            string rule1 = "if(a = tak and b = nie)then(c = tak, d = nie)";
            string rule2 = "if(0.5)then(c = tak)";
            string rule3 = "if(0.2)then(d = tak)";
            string rule4 = "if(a = nie and ( not b = nie or 0.5))then(c = nie, d = nie)";

            ruleSet.AddRule(rule1);
            ruleSet.AddRule(rule2);
            ruleSet.AddRule(rule3);
            ruleSet.AddRule(rule4);
            List<List<double>> result = ruleSet.AggragateRules(new List<double> { 0.2, 0.4 });
            Assert.That(result[0][0], Is.EqualTo(0.5));
        }
        [Test]
        public void AggregateRules_TwoArguments_1and2ResultIs0point5()
        {
            RuleSet ruleSet = new RuleSet(new List<LinguisticVariable>(), new List<LinguisticVariable>());
            MembershipFunction f1 = new Triangle(new List<double>() { 0, 0, 1 }, "nie");
            MembershipFunction f2 = new Triangle(new List<double>() { 0, 1, 1 }, "tak");
            LinguisticVariable lv1 = new LinguisticVariable();
            lv1.Name = "a";
            lv1.MembershipFunctions.Add(f1);
            lv1.MembershipFunctions.Add(f2);
            LinguisticVariable lv2 = new LinguisticVariable(lv1, "b");
            LinguisticVariable lv3 = new LinguisticVariable(lv1, "c");
            LinguisticVariable lv4 = new LinguisticVariable(lv1, "d");
            ruleSet.Outputs.Add(lv3);
            ruleSet.Outputs.Add(lv4);
            ruleSet.Inputs.Add(lv1);
            ruleSet.Inputs.Add(lv2);


            string rule1 = "if(a = tak and b = nie)then(c = tak, d = nie)";
            string rule2 = "if(0.5)then(c = tak)";
            string rule3 = "if(0.2)then(d = tak)";
            string rule4 = "if(a = nie and ( not b = nie or 0.5))then(c = nie, d = nie)";

            ruleSet.AddRule(rule1);
            ruleSet.AddRule(rule2);
            ruleSet.AddRule(rule3);
            ruleSet.AddRule(rule4);
            List<List<double>> result = ruleSet.AggragateRules(new List<double> { 0.2, 0.4 });
            Assert.That(result[0][1], Is.EqualTo(0.5));
        }
        [Test]
        public void AggregateRules_TwoArguments_2and1ResultIs0point5()
        {
            RuleSet ruleSet = new RuleSet(new List<LinguisticVariable>(), new List<LinguisticVariable>());
            MembershipFunction f1 = new Triangle(new List<double>() { 0, 0, 1 }, "nie");
            MembershipFunction f2 = new Triangle(new List<double>() { 0, 1, 1 }, "tak");
            LinguisticVariable lv1 = new LinguisticVariable();
            lv1.Name = "a";
            lv1.MembershipFunctions.Add(f1);
            lv1.MembershipFunctions.Add(f2);
            LinguisticVariable lv2 = new LinguisticVariable(lv1, "b");
            LinguisticVariable lv3 = new LinguisticVariable(lv1, "c");
            LinguisticVariable lv4 = new LinguisticVariable(lv1, "d");
            ruleSet.Outputs.Add(lv3);
            ruleSet.Outputs.Add(lv4);
            ruleSet.Inputs.Add(lv1);
            ruleSet.Inputs.Add(lv2);


            string rule1 = "if(a = tak and b = nie)then(c = tak, d = nie)";
            string rule2 = "if(0.5)then(c = tak)";
            string rule3 = "if(0.2)then(d = tak)";
            string rule4 = "if(a = nie and ( not b = nie or 0.5))then(c = nie, d = nie)";

            ruleSet.AddRule(rule1);
            ruleSet.AddRule(rule2);
            ruleSet.AddRule(rule3);
            ruleSet.AddRule(rule4);
            List<List<double>> result = ruleSet.AggragateRules(new List<double> { 0.2, 0.4 });
            Assert.That(result[1][0], Is.EqualTo(0.5));
        }
        [Test]
        public void AggregateRules_TwoArguments_2and2ResultIs0point5()
        {
            RuleSet ruleSet = new RuleSet(new List<LinguisticVariable>(), new List<LinguisticVariable>());
            MembershipFunction f1 = new Triangle(new List<double>() { 0, 0, 1 }, "nie");
            MembershipFunction f2 = new Triangle(new List<double>() { 0, 1, 1 }, "tak");
            LinguisticVariable lv1 = new LinguisticVariable();
            lv1.Name = "a";
            lv1.MembershipFunctions.Add(f1);
            lv1.MembershipFunctions.Add(f2);
            LinguisticVariable lv2 = new LinguisticVariable(lv1, "b");
            LinguisticVariable lv3 = new LinguisticVariable(lv1, "c");
            LinguisticVariable lv4 = new LinguisticVariable(lv1, "d");
            ruleSet.Outputs.Add(lv3);
            ruleSet.Outputs.Add(lv4);
            ruleSet.Inputs.Add(lv1);
            ruleSet.Inputs.Add(lv2);


            string rule1 = "if(a = tak and b = nie)then(c = tak, d = nie)";
            string rule2 = "if(0.5)then(c = tak)";
            string rule3 = "if(0.2)then(d = tak)";
            string rule4 = "if(a = nie and ( not b = nie or 0.5))then(c = nie, d = nie)";

            ruleSet.AddRule(rule1);
            ruleSet.AddRule(rule2);
            ruleSet.AddRule(rule3);
            ruleSet.AddRule(rule4);
            List<List<double>> result = ruleSet.AggragateRules(new List<double> { 0.2, 0.4 });
            Assert.That(result[1][1], Is.EqualTo(0.2));
        }
    }
}
