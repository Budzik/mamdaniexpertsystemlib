﻿using MamdaniExpertSystemLib.Operators.OneArgumentOperators.Concrete.Negation;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.TNormOperators;
using MamdaniExpertSystemLib.Rules;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.Test
{
    [TestFixture]
    class RuleTests
    {
        [Test]
        public void BuildRule_StringWithoutBracketsAndConst_ThirdElementIsPlus()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1+iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.Inputs[2].ToString();
            Assert.That(result, Is.EqualTo("+"));
        }
        [Test]
        public void BuildRule_StringWithoutBracketsAndConst_NumberOfElementsIs7()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1+iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            int result = rule.Inputs.Count;
            Assert.That(result, Is.EqualTo(7));
        }
        [Test]
        public void BuildRule_StringWithoutBracketsAndConst_SecondElementIsIv1a2()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1+iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.Inputs.ElementAt(1).ToString();
            Assert.That(result, Is.EqualTo("iv1a2"));
        }
        [Test]
        public void BuildRule_StringWithoutBracketsAndWithConstant_SecondElementC2()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+0.2+iv2a1+iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.Inputs.ElementAt(1).ToString();
            Assert.That(result, Is.EqualTo("0.2"));
        }
        [Test]
        public void BuildRule_StringWithBracket_SecondElementIv1a2()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*(iv1a2+iv2a2)*0.3+0.4)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.Inputs.ElementAt(1).ToString();
            Assert.That(result, Is.EqualTo("iv1a2"));
        }
        [Test]
        public void BuildRule_StringWithBracket_FourthElementPlus()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*(iv1a2+iv2a2)*0.3+0.4)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.Inputs.ElementAt(3).ToString();
            Assert.That(result, Is.EqualTo("+"));
        }
        [Test]
        public void BuildRule_StringWithBracket_FifthElementStar()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*(iv1a2+iv2a2)*0.3+0.4)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.Inputs.ElementAt(4).ToString();
            Assert.That(result, Is.EqualTo("*"));
        }
        [Test]
        public void BuildRule_StringWithBracket_SixthElementC1()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*(iv1a2+iv2a2)*0.3+0.4)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.Inputs.ElementAt(5).ToString();
            Assert.That(result, Is.EqualTo("0.3"));
        }
        [Test]
        public void BuildRule_StringWithoutBracketsAndConst_FirstOutputIsOv2a1()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1+iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.Outputs.ElementAt(0).ToString();
            Assert.That(result, Is.EqualTo("ov2a1"));
        }
        [Test]
        public void BuildRule_BadString1_ArgumentsException()
        {
            Rule rule = new Rule();
            string text = "blablablabla";
            Assert.Throws(typeof(ArgumentException), () => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_BadString2_ArgumentsException()
        {
            Rule rule = new Rule();
            string text = "if(blavla)then(ov2a1)";
            Assert.Throws(typeof(ArgumentException), () => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_BadString3_ArgumentsException()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1)then(blabla)";
            Assert.Throws(typeof(ArgumentException), () => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_BadString4_ArgumentsException()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+vi2a1+iv2a2)then(ov2a1)";
            Assert.Throws(typeof(ArgumentException), () => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_BadString5_ArgumentsException()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1+iv2a2)then(vo2a1, ov1a1)";
            Assert.Throws(typeof(ArgumentException), () => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_CorrectString_NoException()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1)then(ov2a1)";
            Assert.DoesNotThrow(() => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_CorrectString1_NoException()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2*(~0.1*0.2))then(ov2a1, ov1a1)";
            Assert.DoesNotThrow(() => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_StringWithNegationBracketAndConst_StackIsCorrect()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*~iv1a2*(0.1*~0.2))then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            int result = rule.Inputs.Count;
            Assert.That(result, Is.EqualTo(9));
        }
        [Test]
        public void BuildRule_StringWithNegationBracketAndConst1_3rdElementIsNegation()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+(~iv1a2*~iv1a1+0.1*0.2*~(~0.1+~0.2)))then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            List<ElementOfExpression> inputs = new List<ElementOfExpression>(rule.Inputs);
            string element = inputs[2].ToString();
            Assert.That(element, Is.EqualTo("~"));
        }
        [Test]
        public void BuildRule_StringWithNegationBracketAndConst1_4thElementIsiv1a1()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+(~iv1a2*~iv1a1+0.1*0.2*~(~0.1+~0.2)))then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            List<ElementOfExpression> inputs = new List<ElementOfExpression>(rule.Inputs);
            string element = inputs[3].ToString();
            Assert.That(element, Is.EqualTo("iv1a1"));
        }
        [Test]
        public void BuildRule_StringWithNegationBracketAndConst1_9thElementIsAnd()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+(~iv1a2*~iv1a1+0.1*0.2*~(~0.1+~0.2)))then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            List<ElementOfExpression> inputs = new List<ElementOfExpression>(rule.Inputs);
            string element = inputs[8].ToString();
            Assert.That(element, Is.EqualTo("*"));
        }
        [Test]
        public void BuildRule_StringWithNegationBracketAndConst1_12thElementIsConst()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+(~iv1a2*~iv1a1+0.1*0.2*~(~0.1+~0.2)))then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            List<ElementOfExpression> inputs = new List<ElementOfExpression>(rule.Inputs);
            string element = inputs[11].ToString();
            Assert.That(element, Is.EqualTo("0.2"));
        }
        [Test]
        public void BuildRule_StringWithNegationBracketAndConst1_15thElementIsNegation()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+(~iv1a2*~iv1a1+0.1*0.2*~(~0.1+~0.2)))then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            List<ElementOfExpression> inputs = new List<ElementOfExpression>(rule.Inputs);
            string element = inputs[14].ToString();
            Assert.That(element, Is.EqualTo("~"));
        }
        [Test]
        public void BuildRule_StringWithNegationBracketAndConst1_14thElementIsOr()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+(~iv1a2*~iv1a1+0.1*0.2*~(~0.1+~0.2)))then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            List<ElementOfExpression> inputs = new List<ElementOfExpression>(rule.Inputs);
            string element = inputs[13].ToString();
            Assert.That(element, Is.EqualTo("+"));
        }
        [Test]
        public void BuildRule_CorrectString2_NoException()
        {
            Rule rule = new Rule();
            string text = "If(IV1a1+iv1A2*(~0.1*0.2))tHeN(oV2A1, Ov1a1)";
            Assert.DoesNotThrow(() => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_StringWithBracket_NumberOfElementsIs9()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*(iv1a2+iv2a2)*0.3+0.4)then(ov2a1)";
            rule.BuildRule(text);
            int result = rule.Inputs.Count;
            Assert.That(result, Is.EqualTo(9));
        }
        [Test]
        public void BuildRule_StringWith3Outputs_NumberOfOutputsIs3()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*(iv1a2+iv2a2)*0.3+0.4)then(ov2a1, ov2a1, ov2a1)";
            rule.BuildRule(text);
            int result = rule.Outputs.Count;
            Assert.That(result, Is.EqualTo(3));
        }
        [Test]
        public void BuildRule_StringWithEmptyBracket_ArgumentException()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+vi2a1+()+iv2a2)then(ov2a1)";
            Assert.Throws(typeof(ArgumentException), () => rule.BuildRule(text));
        }
        [Test]
        public void BuildRule_StringWithDoubleOperators_ArgumentException()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+vi2a1++iv2a2)then(ov2a1)";
            Assert.Throws(typeof(ArgumentException), () => rule.BuildRule(text));
        }
        [Test]
        public void ShowRule_SimpleExpressionOnlyWithAnds_CorrectStringInOutput()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1+iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.ShowRule();
            Assert.That(result, Is.EqualTo(text));
        }
        [Test]
        public void ShowRule_SimpleExpressionWithAndsAndOr_CorrectStringInOutput()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1*iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.ShowRule();
            Assert.That(result, Is.EqualTo(text));
        }
        [Test]
        public void ShowRule_SimpleExpressionWithNegation_CorrectStringInOutput()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+~iv1a2+iv2a1*iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.ShowRule();
            Assert.That(result, Is.EqualTo(text));
        }
        [Test]
        public void ShowRule_SimpleExpressionWithBracket_CorrectStringInOutput()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*(iv1a2+iv2a1))then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.ShowRule();
            Assert.That(result, Is.EqualTo(text));
        }
        [Test]
        public void ShowRule_SimpleExpressionWithUselessBracket_CorrectStringInOutput()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+(iv1a2*iv2a1))then(ov2a1)";
            rule.BuildRule(text);
            string result = rule.ShowRule();
            Assert.That(result, Is.EqualTo("if(iv1a1+iv1a2*iv2a1)then(ov2a1)"));
        }
        [Test]
        public void ShowRule_SimpleExpressionWithNegationsBracketEtc_CorrectStringInOutput()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1*(iv2a1+iv2a2*0.3+~(~0.1+0.2)*0.9)+~0.3)then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            string result = rule.ShowRule();
            Assert.That(result, Is.EqualTo(text));
        }
        [Test]
        public void ShowRule_NotSimpleExample_CorrectStringInOutput()
        {
            Rule rule = new Rule();
            string text = "if(0.1+(0.1*~(0.2*0.3+~0.4+~(0.2+0.3+0.4))+~0.1))then(ov2a1, ov1a1)";
            rule.BuildRule(text);
            string result = rule.ShowRule();
            Assert.That(result, Is.EqualTo(text));
        }
        [Test]
        public void DesignateValue_SimpleRuleOnly1And0AsArgument_CorrectValue()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1+iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            List<List<double>> membershipDegree = new List<List<double>>() { new List<double> { 1, 0 }, new List<double> { 0, 1 } };
            double result = rule.DesignateValue(membershipDegree, new MulTNorm(), new MaxSNorm(), new NormalNegation());
            Assert.That(result, Is.EqualTo(1));
        }
        [Test]
        public void DesignateValue_SimpleRule_CorrectValue()
        {
            Rule rule = new Rule();
            string text = "if(iv1a1+iv1a2+iv2a1+iv2a2)then(ov2a1)";
            rule.BuildRule(text);
            List<List<double>> membershipDegree = new List<List<double>>() { new List<double> { 0.9, 0.3 }, new List<double> { 0.4, 0.3 } };
            double result = rule.DesignateValue(membershipDegree, new MulTNorm(), new MaxSNorm(), new NormalNegation());
            Assert.That(result, Is.EqualTo(0.9));
        }
        [Test]
        public void DesignateValue_ComplicatedRuleWith0And1AsArgument_CorrectValue()
        {
            Rule rule = new Rule();
            string text = "if(~iv1a1+(1*(~iv1a2*~(1+iv1a2))))then(ov2a1)";
            rule.BuildRule(text);
            List<List<double>> membershipDegree = new List<List<double>>() { new List<double> { 1 , 0 }};
            double result = rule.DesignateValue(membershipDegree, new MulTNorm(), new MaxSNorm(), new NormalNegation());
            Assert.That(result, Is.EqualTo(0));
        }
        [Test]
        public void DesignateValue_ComplicatedRule_CorrectValue()
        {
            Rule rule = new Rule();
            string text = "if(~iv1a1+(1*(~iv1a2*~(1+iv1a2))))then(ov2a1)";
            rule.BuildRule(text);
            List<List<double>> membershipDegree = new List<List<double>>() { new List<double> { 0.3, 0.9 } };
            double result = rule.DesignateValue(membershipDegree, new MulTNorm(), new MaxSNorm(), new NormalNegation());
            Assert.That(result, Is.EqualTo(0.7));
        }
        [Test]
        public void DesignateValue_VeryComplicatedRuleWith0And1AsArgument_CorrectValue()
        {
            Rule rule = new Rule();
            string text = "if(~iv1a1+(iv1a2*(iv2a1+~(iv2a2*iv3a1+(iv3a2*iv1a1))+iv2a1)))then(ov2a1)";
            rule.BuildRule(text);
            List<List<double>> membershipDegree = new List<List<double>>() { new List<double> { 1, 1 }, new List<double> { 0, 0 }, new List<double> { 1, 1 } };
            double result = rule.DesignateValue(membershipDegree, new MulTNorm(), new MaxSNorm(), new NormalNegation());
            Assert.That(result, Is.EqualTo(0));
        }
        [Test]
        public void DesignateValue_VeryComplicatedRule_CorrectValue()
        {
            Rule rule = new Rule();
            string text = "if(~iv1a1+(iv1a2*(iv2a1+~(iv2a2*iv3a1+(iv3a2*iv1a1))+iv2a1)))then(ov2a1)";
            rule.BuildRule(text);
            List<List<double>> membershipDegree = new List<List<double>>() { new List<double> { 0.7, 0.4 }, new List<double> { 0.6, 0.9 }, new List<double> { 0.2, 0.8 } };
            double result = rule.DesignateValue(membershipDegree, new MulTNorm(), new MaxSNorm(), new NormalNegation());
            Assert.AreEqual(0.3, result, 0.000001);
        }
        [Test]
        public void DesignateValue_VeryComplicatedRule1_CorrectValue()
        {
            Rule rule = new Rule();
            string text = "if(~iv1a1+(iv1a2*(iv2a1+~(iv2a2*iv3a1+(iv3a2*iv1a1))+iv2a1)))then(ov2a1)";
            rule.BuildRule(text);
            List<List<double>> membershipDegree = new List<List<double>>() { new List<double> { 0.9, 1 }, new List<double> { 0.2, 0.1 }, new List<double> { 0.1, 0 } };
            double result = rule.DesignateValue(membershipDegree, new MulTNorm(), new MaxSNorm(), new NormalNegation());
            Assert.That(result, Is.EqualTo(0.99));
        }
       
    }
}
