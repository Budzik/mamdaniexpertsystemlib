﻿using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Defuzzyfication.Abstract
{
    public interface IDefuzzyfication
    {
        string Name { get; }
        double DesignateValue(List<double> arguments, List<double> values);
    }
}
