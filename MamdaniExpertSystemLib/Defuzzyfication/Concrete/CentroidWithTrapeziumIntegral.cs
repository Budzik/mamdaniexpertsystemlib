﻿using MamdaniExpertSystemLib.Defuzzyfication.Abstract;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Defuzzyfication.Concrete
{
    public class CentroidWithTrapeziumIntegral : IDefuzzyfication
    {
        public string Name
        {
            get
            {
                return "CentroidWithTrapeziumIntegral";
            }
        }

        public double DesignateValue(List<double> arguments, List<double> values)
        {
            List<double> temp = new List<double>();
            for (int i = 0; i < arguments.Count; i++)
            {
                temp.Add(arguments[i] * values[i]);
            }
            double temp3 = Integrate(arguments, temp);
            double temp4 = Integrate(arguments, values);
            double temp1 = temp3 / temp4;
            if(temp3 == 0 && temp4 == 0)
            {
                double temp2 = 0;
                foreach (double item in arguments)
                {
                    temp2 += item;
                }
                return temp2 / arguments.Count;
            }
            
            return temp1;
        }

        private double Integrate(List<double> arguments, List<double> values)
        {
            double value = 0;
            for (int i = 0; i < arguments.Count - 1; i++)
            {
                value += Math.Abs(arguments[i + 1] - arguments[i]) * (values[i] + values[i + 1]) / 2;
            }
            return value;
        }
    }
}
