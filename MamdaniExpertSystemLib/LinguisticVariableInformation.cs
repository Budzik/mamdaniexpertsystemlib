﻿using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib
{
    public class LinguisticVariableInformation
    {
        public string Name { get; set; }
        public List<MembershipFunctionInformation> Functions { get; set; }
        public double LowerLimit { get; set; }
        public double UpperLimit { get; set; }

        public LinguisticVariableInformation()
        {
            Functions = new List<MembershipFunctionInformation>();
        }

        public void SetInformation(LinguisticVariable lv)
        {
            Name = lv.Name;
            foreach(MembershipFunction item in lv.MembershipFunctions)
            {
                MembershipFunctionInformation temp = new MembershipFunctionInformation();
                temp.SetFunction(item);
                Functions.Add(temp);
            }
        }
        public LinguisticVariable GetLinguisticVariable()
        {
            LinguisticVariable lv = new LinguisticVariable();
            lv.Name = Name;
            lv.LowerLimitOfRange = this.LowerLimit;
            lv.UpperLimitOfRange = this.UpperLimit;
            foreach (MembershipFunctionInformation item in Functions)
            {
                lv.AddMembershipFunction(item.getMembershipFunction());
            }
            return lv;
        }
    }
}
