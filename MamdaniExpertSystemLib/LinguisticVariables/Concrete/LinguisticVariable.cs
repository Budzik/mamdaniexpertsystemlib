﻿using MamdaniExpertSystemLib.MembershipFunctions;
using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace MamdaniExpertSystemLib.LinguisticVariables.Concrete
{
    public class LinguisticVariable
    {
        public List<MembershipFunction> MembershipFunctions { get; private set; }
        private string name;
        private double upperLimitOfRange;
        private double lowerLimitOfRange;
        public double UpperLimitOfRange { get { return upperLimitOfRange; } set { 
                upperLimitOfRange = value;
            } }
        public double LowerLimitOfRange { get { return lowerLimitOfRange; } set {
                lowerLimitOfRange = value;                
            } }
        public EventHandler OnRuleDelete;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value.ToLower();
                if (name.Contains(" "))
                {
                    throw new ArgumentException("Linguistic variable name can't contain spaces.");
                }
                else if (name.Contains("+"))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \"+\" characters.");
                }
                else if (name.Contains("*"))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \"*\" characters.");
                }
                else if (name.Contains("~"))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \"~\" characters.");
                }
                else if (name.Contains("."))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \".\" characters.");
                }
                else if (name.Contains("!"))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \"!\" characters.");
                }
                else if (name.Contains("@"))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \",\" characters.");
                }
                else if (name.Contains("("))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \",\" characters.");
                }
                else if (name.Contains(")"))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \",\" characters.");
                }
                else if (name.Contains("="))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \",\" characters.");
                }
                else if (name.Contains(","))
                {
                    throw new ArgumentException("Linguistic variable name can't contain \",\" characters.");
                }
                else if(name[0] >= '0' && name[0] <= '9')
                {
                    throw new ArgumentException("Linguistic variable name can't start with number.");
                }
                string n = name.ToLower();
                if (n.Equals("not") || n.Equals("or") || n.Equals("and") || n.Equals("is"))
                {
                    throw new ArgumentException("This name of linguistic variable is forbidden.");
                }                
            }
        }
        public int NumberOfMembershipFunctions { get { return MembershipFunctions.Count; } }
        public LinguisticVariable()
        {
            MembershipFunctions = new List<MembershipFunction>();
            LowerLimitOfRange = 0;
            UpperLimitOfRange = 1;
        }
        public LinguisticVariable(LinguisticVariable linguisticVariable, string name)
        {
            this.MembershipFunctions = new List<MembershipFunction>();
            this.UpperLimitOfRange = linguisticVariable.UpperLimitOfRange;
            this.LowerLimitOfRange = linguisticVariable.LowerLimitOfRange;
            this.Name = name;
            foreach (MembershipFunction membershipFunction in linguisticVariable.MembershipFunctions)
            {
                int number = MembershipFunctionFactory.GetNumberOfFunction(membershipFunction.TypeOfMembershipFunction);
                MembershipFunction exampleFunction = MembershipFunctionFactory.CreateMembershipFunction((TypeOfMembershipFunctions)number, membershipFunction.Parameters, membershipFunction.Name);
                this.AddMembershipFunction(exampleFunction);
            }
        }
        public Dictionary<string, double> DesignateMembershipGrades(double arg)
        {
            Dictionary<string, double> temp = new Dictionary<string, double>();
            if (arg >= LowerLimitOfRange && arg <= UpperLimitOfRange)
            {
                foreach (MembershipFunction membershipFunction in MembershipFunctions)
                {
                    temp.Add(membershipFunction.Name, membershipFunction.DesignateMembershipGradeOfFuzzySet(arg));
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException("Argument is out of range.");
            }
            return temp;
        }
        public List<double> DesignateMembershipGradesToList(double arg)
        {
            List<double> temp = new List<double>();
            if (arg >= LowerLimitOfRange && arg <= UpperLimitOfRange)
            {
                foreach (MembershipFunction membershipFunction in MembershipFunctions)
                {
                    temp.Add(membershipFunction.DesignateMembershipGradeOfFuzzySet(arg));
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException("Argument is out of range.");
            }
            return temp;
        }
        public List<string> GetMembershipFunctions()
        {
            List<string> membershipFunctionsDescribingList = new List<string>();
            foreach (MembershipFunction membershipFunction in MembershipFunctions)
            {
                membershipFunctionsDescribingList.Add(membershipFunction.DescribeMembershipFunction());
            }
            return membershipFunctionsDescribingList;
        }
        public void AddMembershipFunction(MembershipFunction membershipFunction)
        {
            bool IsExist;
            try
            {
                MembershipFunction mf = FindMembershipFunction(membershipFunction.Name);
                IsExist = true;
            }
            catch
            {
                IsExist = false;
            }
            if (!IsExist)
            {
                MembershipFunctions.Add(membershipFunction);
            }
            else
            {
                throw new ArgumentException("Function named " + membershipFunction.Name + " is exist. Change the name of this membership function.");
            }
        }
        public void RemoveMembershipFunction(int numberOfMembershipFunction)
        {
            MembershipFunctions.RemoveAt(numberOfMembershipFunction);
            OnRuleDelete(this, new LinguisticVariableEventArg(numberOfMembershipFunction));
        }
        public void RemoveMembershipFunction(string name)
        {
            MembershipFunction membershipFunction = this.FindMembershipFunction(name);
            if (membershipFunction != null)
            {
                RemoveMembershipFunction(membershipFunction);
                OnRuleDelete(this, new LinguisticVariableEventArg(FindNumberOfMembershipFunction(name)));
            }
        }
        public void RemoveMembershipFunction(MembershipFunction membershipFunction)
        {
            try
            {
                MembershipFunctions.Remove(membershipFunction);
                OnRuleDelete(this, new LinguisticVariableEventArg(FindNumberOfMembershipFunction(membershipFunction.Name)));
            }
            catch
            {
                throw new ArgumentException("This membership function is not exist.");
            }
        }
        public void EditMembershipFunction(int number, MembershipFunction membershipFunction)
        {
            this.MembershipFunctions[number] = membershipFunction;
        }
        public void EditMembershipFunction(string name, MembershipFunction membershipFunction)
        {
            this.MembershipFunctions[this.FindNumberOfMembershipFunction(name)] = membershipFunction;
        }
        public MembershipFunction FindMembershipFunction(string name)
        {
            foreach (MembershipFunction membershipFunction in MembershipFunctions)
            {
                if (membershipFunction.Name.Equals(name))
                {
                    return membershipFunction;
                }
            }
            throw new ArgumentException("Membership function named " + name + " is not exist.");
        }
        public int FindNumberOfMembershipFunction(string name)
        {
            int i = 0;
            foreach (MembershipFunction membershipFunction in MembershipFunctions)
            {
                if (membershipFunction.Name.Equals(name))
                {
                    return i;
                }
                i++;
            }
            throw new ArgumentException("Membership function named " + name + " is not exist.");
        }
        public int FindNumberOfMembershipFunction(MembershipFunction membershipFunction)
        {
            int i = 0;
            foreach (MembershipFunction membershipFunction1 in MembershipFunctions)
            {
                if (membershipFunction == membershipFunction1)
                {
                    return i;
                }
                i++;
            }
            throw new ArgumentException("This membership function not exist in collection. ");
        }
        public string CompleteMembershipFunctionName(string text)
        {
            foreach (MembershipFunction item in MembershipFunctions)
            {
                int i;
                for(i = 0; i < text.Length; i++)
                {
                    if(text[i] != item.Name[i])
                    {
                        break;
                    }
                }
                if(i == text.Length && !item.Name.Equals(text))
                {
                    return item.Name;
                }
            }
            return null;
        }


    }
}
