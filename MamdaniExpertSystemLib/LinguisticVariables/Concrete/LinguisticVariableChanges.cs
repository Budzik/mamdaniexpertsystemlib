﻿namespace MamdaniExpertSystemLib.LinguisticVariables.Concrete
{
    public enum LinguisticVariableChanges
    {
        AddFunction,
        RemoveFunction,
        EditFunction,
        ChangeLowerLimitOfRange,
        ChangeUpperLimitOfRange,
        ChangeName
    }
}
