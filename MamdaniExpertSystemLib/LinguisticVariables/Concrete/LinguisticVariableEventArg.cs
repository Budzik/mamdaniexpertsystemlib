﻿using System;

namespace MamdaniExpertSystemLib.LinguisticVariables.Concrete
{
    public class LinguisticVariableEventArg : EventArgs
    {
        public int FunctionIndex { get; set; }
        public LinguisticVariableEventArg(int functionIndex)
        {
            this.FunctionIndex = functionIndex;
        }
    }
}
