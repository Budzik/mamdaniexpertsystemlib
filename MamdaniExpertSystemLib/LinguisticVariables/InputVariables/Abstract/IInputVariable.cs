﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.LinguisticVariables.InputVariables.Abstract
{
    public interface IInputVariable
    {
        List<Double> Fuzzyficate(double arg);
    }
}
