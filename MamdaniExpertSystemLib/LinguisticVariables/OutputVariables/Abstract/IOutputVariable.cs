﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.LinguisticVariables.OutputVariables.Abstract
{
    public interface IOutputVariable
    {
        List<Double> Fuzzyficate(double arg);
    }
}
