﻿using MamdaniExpertSystemLib.Defuzzyfication.Abstract;
using MamdaniExpertSystemLib.Defuzzyfication.Concrete;
using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.TNormOperators;
using MamdaniExpertSystemLib.Rules;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib
{
    public class MamdaniExpertSystem
    {
        public List<LinguisticVariable> Inputs { get; private set; }
        public List<LinguisticVariable> Outputs { get; private set; }
        public IDefuzzyfication Defuzzyfication { get; set; }
        public TwoArgumentsOperator Implication { get; set; }
        public EventHandler OnInputDelete;
        public EventHandler OnOutputDelete;
        public RuleSet ruleSet { get; private set; }
        public int precission { get; set; }
        public int OutputsNumber { get { return Outputs.Count; } }
        public int InputsNumber { get { return Inputs.Count; } }
        public int RuleNumber { get { return ruleSet.RulesNumber; } }
        public MamdaniExpertSystem()
        {
            Inputs = new List<LinguisticVariable>();
            Outputs = new List<LinguisticVariable>();
            ruleSet = new RuleSet(Inputs, Outputs);
            OnInputDelete += RepairRulesAfterDeleteInputLinguisticVariable;
            OnOutputDelete += RepairRulesAfterDeleteOutputLinguisticVariable;
            Defuzzyfication = new CentroidWithTrapeziumIntegral();
            Implication = new MinTNorm();
            precission = 200;
        }
        private void RepairRulesAfterDeleteOutputLinguisticVariable(object sender, EventArgs e)
        {
            int parameter = (e as LinguisticVariableEventArg).FunctionIndex;
            ruleSet.RepairAfterDeleteOutputLinguisticVariable(parameter);
        }
        private void RepairRulesAfterDeleteInputLinguisticVariable(object sender, EventArgs e)
        {
            int parameter = (e as LinguisticVariableEventArg).FunctionIndex;
            ruleSet.RepairAfterDeleteInputLinguisticVariable(parameter);
        }
        public int FindInputByName(string name)
        {
            int i = 0;
            foreach (LinguisticVariable linguisticVariable in Inputs)
            {
                if (linguisticVariable.Name.Equals(name))
                {
                    return i;
                }
                i++;
            }
            return -1;
        }
        public void AddInput(LinguisticVariable linguisticVariable)
        {
            if(FindInputByName(linguisticVariable.Name) == -1 && linguisticVariable.Name.Length != 0)
            {
                Inputs.Add(linguisticVariable);
                linguisticVariable.OnRuleDelete += RepairRulesAfterDeleteInputMembershipFunction;
            }
            else
            {
                throw new ArgumentException("Input linguistic variable named the same is exist or you try to add linguistic variable without name.");
            }
        }
        private void RepairRulesAfterDeleteInputMembershipFunction(object sender, EventArgs e)
        {
            int i = FindInputByName((sender as LinguisticVariable).Name);
            int j = (e as LinguisticVariableEventArg).FunctionIndex;
            ruleSet.RepairAfterDeleteInputFunction(i, j);
        }
        public void RemoveInput(int number)
        {
            Inputs.RemoveAt(number);
            OnInputDelete(this, new LinguisticVariableEventArg(number));
        }
        public void RemoveInput(string name)
        {
            if (FindInputByName(name) != -1)
            {
                int i = FindInputByName(name);
                Inputs.Remove(Inputs[FindInputByName(name)]);
                OnInputDelete(this, new LinguisticVariableEventArg(i));
            }
            else
            {
                throw new ArgumentException("Input linguistic variable named the same is exist or you try to add linguistic variable without name.");
            }
        }
        public void RemoveInput(LinguisticVariable linguisticVariable)
        {
            int i = FindInputByName(linguisticVariable.Name);
            Inputs.Remove(linguisticVariable);
            OnInputDelete(this, new LinguisticVariableEventArg(FindInputByName(linguisticVariable.Name)));
        }
        public void EditInput(int number, LinguisticVariable linguisticVariable)
        {
            Inputs[number] = linguisticVariable;
        }
        public void EditInput(string name, LinguisticVariable linguisticVariable)
        {
            int number = FindInputByName(name);
            if(number != -1)
            {
                Inputs[number] = linguisticVariable;
            }
            else
            {
                throw new ArgumentException("Input linguistic variable named " + name + " is not exist.");
            }
        }
        public int FindOutputByName(string name)
        {
            int i = 0;
            foreach (LinguisticVariable linguisticVariable in Outputs)
            {
                if (linguisticVariable.Name.Equals(name))
                {
                    return i;
                }
                i++;
            }
            return -1;
        }
        public void AddOutput(LinguisticVariable linguisticVariable)
        {
            if (FindOutputByName(linguisticVariable.Name) == -1 && linguisticVariable.Name.Length != 0)
            {
                Outputs.Add(linguisticVariable);
                linguisticVariable.OnRuleDelete += RepairRulesAfterDeleteOutputMembershipFunction;
            }
            else
            {
                throw new ArgumentException("Output linguistic variable named the same is exist or you try to add linguistic variable without name.");
            }
        }
        private void RepairRulesAfterDeleteOutputMembershipFunction(object sender, EventArgs e)
        {
            int i = FindOutputByName((sender as LinguisticVariable).Name);
            int j = (e as LinguisticVariableEventArg).FunctionIndex;
            ruleSet.RepairAfterDeleteOutputFunction(i, j);
        }
        public void RemoveOutput(int number)
        {
            Outputs.RemoveAt(number);
            OnOutputDelete(this, new LinguisticVariableEventArg(number));
        }
        public void RemoveOutput(string name)
        {
            if (FindOutputByName(name) != -1)
            {
                int i = FindOutputByName(name);
                Outputs.Remove(Outputs[FindOutputByName(name)]);
                OnOutputDelete(this, new LinguisticVariableEventArg(i));
            }
            else
            {
                throw new ArgumentException("Output linguistic variable named the same is exist or you try to add linguistic variable without name.");
            }
        }
        public void RemoveOutput(LinguisticVariable linguisticVariable)
        {
            int i = FindOutputByName(linguisticVariable.Name);
            Outputs.Remove(linguisticVariable);
            OnOutputDelete(this, new LinguisticVariableEventArg(i));
        }
        public void EditOutput(int number, LinguisticVariable linguisticVariable)
        {
            Outputs[number] = linguisticVariable;
        }
        public void EditOutput(string name, LinguisticVariable linguisticVariable)
        {
            int number = FindOutputByName(name);
            if (number != -1)
            {
                Outputs[number] = linguisticVariable;
            }
            else
            {
                throw new ArgumentException("Output linguistic variable named " + name + " is not exist.");
            }
        }
        public List<double> CalculateValue(List<double> arguments)
        {
            List<double> result = new List<double>();
            List<List<double>> minimums = ruleSet.AggragateRules(arguments);
            for (int i = 0; i < Outputs.Count; i++)
            {
                List<List<double>> points = GetPoints(i, minimums[i]);
                if (Defuzzyfication.DesignateValue(points[0], points[1]) != double.NaN)
                {
                    result.Add(Defuzzyfication.DesignateValue(points[0], points[1]));
                }
                else
                {
                    result.Add((Outputs[i].UpperLimitOfRange - Outputs[i].LowerLimitOfRange) / 2);
                }
            }
            return result;

        }
        public double DesignateOutputValue(double arg, int number, List<double> minimums)
        {
            LinguisticVariable output = Outputs[number];
            List<double> result = output.DesignateMembershipGradesToList(arg);
            for (int i = 0; i < result.Count; i++)
            {
                result[i] = Implication.DesignateValue(result[i], minimums[i]);
            }
            double max = 0;
            for (int i = 0; i < result.Count; i++)
            {
                if(result[i] > max)
                {
                    max = result[i];
                }
            }
            return max;
        }
        public double GetOutputFunction(List<double> args, double arg, int number)
        {
            List<List<double>> minimums = ruleSet.AggragateRules(args);
            return DesignateOutputValue(arg, number, minimums[number]);
        }
        private List<List<double>> GetPoints(int number, List<double> minimums)
        {
            LinguisticVariable output = this.Outputs[number];
            List<double> arguments = new List<double>();
            List<double> values = new List<double>();
            double interval = (double)(output.UpperLimitOfRange - output.LowerLimitOfRange) / precission;
            for(double i = output.LowerLimitOfRange; i <= output.UpperLimitOfRange; i += interval)
            {
                arguments.Add(i);
            }
            foreach(double arg in arguments)
            {
                values.Add(this.DesignateOutputValue(arg, number, minimums));
            }
            List<List<double>> result = new List<List<double>>();
            result.Add(arguments);
            result.Add(values);
            return result;
        }
        public string CompleteMembershipFunction(string text, string linguisticVariable, bool inputFlag)
        {
            if (inputFlag)
            {
                int i = FindInputByName(linguisticVariable);
                string n = null;
                if(i != -1)
                {
                    n = Inputs[i].CompleteMembershipFunctionName(text);
                }
                return n;
            }
            else
            {
                int i = FindOutputByName(linguisticVariable);
                string n = null;
                if (i != -1)
                {
                    n = Outputs[i].CompleteMembershipFunctionName(text);
                }
                return n;
            }
        }
        public string CompleteLinguisticVariable(string text, bool InputFlag)
        {
            if (InputFlag)
            {
                foreach (LinguisticVariable item in Inputs)
                {
                    int i;
                    for (i = 0; i < text.Length; i++)
                    {
                        if (text[i] != item.Name[i])
                        {
                            break;
                        }
                    }
                    if (i == text.Length && !item.Name.Equals(text))
                    {
                        return item.Name;
                    }
                }
                return null;
            }
            else
            {
                foreach (LinguisticVariable item in Outputs)
                {
                    int i;
                    for (i = 0; i < text.Length; i++)
                    {
                        if (text[i] != item.Name[i])
                        {
                            break;
                        }
                    }
                    if (i == text.Length && !item.Name.Equals(text))
                    {
                        return item.Name;
                    }
                }
                return null;
            }
        }
    }
}
