﻿using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MamdaniExpertSystemLib
{
    public class MamdaniExpertSystemBuilder
    {
        public List<LinguisticVariableInformation> Inputs { get; set; }
        public List<LinguisticVariableInformation> Outputs { get; set; }
        public List<string> Rules { get; set; }
        public int Precision { get; set; }
        public TNorms tnorm { get; set; }
        public SNorms snorm { get; set; }
        public SNorms aggregation { get; set; }
        public TNorms implication { get; set; }
        public MamdaniExpertSystemBuilder()
        {
            Inputs = new List<LinguisticVariableInformation>();
            Outputs = new List<LinguisticVariableInformation>();
            Rules = new List<string>();
        }
        public MamdaniExpertSystemBuilder(string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MamdaniExpertSystemBuilder));
            using (StreamReader sr = File.OpenText(path))
            {
                XmlReader reader = XmlReader.Create(sr);
                MamdaniExpertSystemBuilder mesb;
                mesb = (MamdaniExpertSystemBuilder)serializer.Deserialize(reader);
                this.Inputs = mesb.Inputs;
                this.Outputs = mesb.Outputs;
                this.Rules = mesb.Rules;
                this.tnorm = mesb.tnorm;
                this.snorm = mesb.snorm;
                this.implication = mesb.implication;
                this.aggregation = mesb.aggregation;
                this.Precision = mesb.Precision;
            }
        }
        public MamdaniExpertSystemBuilder(MamdaniExpertSystem mes)
        {
            Inputs = new List<LinguisticVariableInformation>();
            Outputs = new List<LinguisticVariableInformation>();
            Rules = new List<string>();
            foreach (LinguisticVariable lv in mes.Inputs)
            {
                LinguisticVariableInformation temp = new LinguisticVariableInformation();
                temp.SetInformation(lv);
                Inputs.Add(temp);
                temp.LowerLimit = lv.LowerLimitOfRange;
                temp.UpperLimit = lv.UpperLimitOfRange;
            }
            foreach (LinguisticVariable lv in mes.Outputs)
            {
                LinguisticVariableInformation temp = new LinguisticVariableInformation();
                temp.SetInformation(lv);
                Outputs.Add(temp);
                temp.LowerLimit = lv.LowerLimitOfRange;
                temp.UpperLimit = lv.UpperLimitOfRange;
            }
            Rules = mes.ruleSet.ShowAllRule();
            for (int i = 0; i < Rules.Count; i++)
            {
                Rules[i] = this.ConvertToNormalizeForm(Rules[i]);
            }
            Precision = mes.precission;
            tnorm = TwoArgumentsOperatorGenerator.WhichTNorm(mes.ruleSet.TNorm.Name);
            snorm = TwoArgumentsOperatorGenerator.WhichSNorm(mes.ruleSet.SNorm.Name);
            implication = TwoArgumentsOperatorGenerator.WhichTNorm(mes.Implication.Name);
            aggregation = TwoArgumentsOperatorGenerator.WhichSNorm(mes.ruleSet.Aggregation.Name);
        }
        public MamdaniExpertSystem GenerateMamdaniExpertSystem()
        {
            MamdaniExpertSystem mes = new MamdaniExpertSystem();
            foreach (LinguisticVariableInformation item in Inputs)
            {
                mes.AddInput(item.GetLinguisticVariable());
            }
            foreach (LinguisticVariableInformation item in Outputs)
            {
                mes.AddOutput(item.GetLinguisticVariable());
            }
            foreach (string item in Rules)
            {
                if (CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0] == '.')
                {
                    mes.ruleSet.AddRule(item);
                }
                else
                {
                    string s = item.Replace('.', CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0]);
                    mes.ruleSet.AddRule(s);
                }
            }
            mes.precission = Precision;
            mes.ruleSet.SNorm = TwoArgumentsOperatorGenerator.GetSNorm(snorm);
            mes.ruleSet.TNorm = TwoArgumentsOperatorGenerator.GetTNorm(tnorm);
            mes.ruleSet.Aggregation = TwoArgumentsOperatorGenerator.GetSNorm(aggregation);
            mes.Implication = TwoArgumentsOperatorGenerator.GetTNorm(implication);
            return mes;
        }
        public void SaveToFile(string path)
        {
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            XmlSerializer serializer = new XmlSerializer(typeof(MamdaniExpertSystemBuilder));
            StringWriter s = new StringWriter();
            serializer.Serialize(s, this);
            string temp = s.ToString();
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.Write(temp);
            }
        }
        private string ConvertToNormalizeForm(string rule)
        {
            string s = rule.Split(new string[] { "if(", ")then(" }, StringSplitOptions.RemoveEmptyEntries)[0];
            string s1 = s.Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0], '.');
            return rule.Replace(s, s1);
        }
    }
}
