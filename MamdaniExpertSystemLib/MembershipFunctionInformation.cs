﻿using MamdaniExpertSystemLib.MembershipFunctions;
using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib
{
    public class MembershipFunctionInformation
    {
        public TypeOfMembershipFunctions type { get; set; }
        public List<double> Parameters { get; set; }
        public string FunctionName { get; set; }

        public void SetFunction(MembershipFunction mf)
        {
            type = (TypeOfMembershipFunctions)MembershipFunctionFactory.GetNumberOfFunction(mf.TypeOfMembershipFunction);
            Parameters = mf.Parameters;
            FunctionName = mf.Name;
        }
        public MembershipFunction getMembershipFunction()
        {
            MembershipFunction mf = MembershipFunctionFactory.CreateMembershipFunction(type, Parameters, FunctionName);
            return mf;
        }
    }
}
