﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MamdaniExpertSystemLib.MembershipFunctions.Abstract
{
    public interface IMembershipFunction
    {
        string Name { get; set; }
        List<double> Parameters { get; set; }
        int NumberOfArguments { get; }
        bool IncreasingFlag { get; }
        double DesignateMembershipGradeOfFuzzySet(double arg);
        void SetParameters(List<double> parameters);
        List<double> GetParameters();
    }
}
