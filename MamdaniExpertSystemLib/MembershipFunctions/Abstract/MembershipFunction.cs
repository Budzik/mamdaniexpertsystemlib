﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MamdaniExpertSystemLib.MembershipFunctions.Abstract
{
    public abstract class MembershipFunction : IMembershipFunction
    {
        protected List<double> parameters;
        protected string name;
        public abstract bool IncreasingFlag { get; }
        public string Name { get {

                return name;
            } set
            {
                name = value.ToLower();
                if (name.Contains(" "))
                {
                    throw new ArgumentException("Membership function name can't contain spaces.");
                }
                else if (name.Contains("+"))
                {
                    throw new ArgumentException("Membership function name can't contain \"+\" characters.");
                }
                else if (name.Contains("*"))
                {
                    throw new ArgumentException("Membership function name can't contain \"*\" characters.");
                }
                else if (name.Contains("~"))
                {
                    throw new ArgumentException("Membership function name can't contain \"~\" characters.");
                }
                else if (name.Contains("."))
                {
                    throw new ArgumentException("Membership function name can't contain \".\" characters.");
                }
                else if (name.Contains("!"))
                {
                    throw new ArgumentException("Membership function name can't contain \"!\" characters.");
                }
                else if (name.Contains("@"))
                {
                    throw new ArgumentException("Membership function name can't contain \",\" characters.");
                }
                else if (name.Contains("("))
                {
                    throw new ArgumentException("Membership function name can't contain \",\" characters.");
                }
                else if (name.Contains(")"))
                {
                    throw new ArgumentException("Membership function name can't contain \",\" characters.");
                }
                else if (name.Contains("="))
                {
                    throw new ArgumentException("Membership function name can't contain \",\" characters.");
                }
                else if (name.Contains(","))
                {
                    throw new ArgumentException("Membership function name can't contain \",\" characters.");
                }
                else if (name[0] >= '0' && name[0] <= '9')
                {
                    throw new ArgumentException("Linguistic variable name can't start with number.");
                }
                string n = name.ToLower();
                if (n.Equals("not") || n.Equals("or") || n.Equals("and") || n.Equals("is"))
                {
                    throw new ArgumentException("This name of membership function is forbidden.");
                }
            }
        }
        public abstract int NumberOfArguments { get; }
        public abstract string TypeOfMembershipFunction { get; }
        public MembershipFunction(List<double> parameters, string name)
        {
            this.parameters = parameters;
            this.Name = name;
        }
        public MembershipFunction()
        {
            parameters = new List<double>();
        }
        public List<double> Parameters
        {
            get
            {
                return this.parameters;
            }

            set
            {
                this.ValidateMembershipFunction(value);
                parameters = value;
            }
        }
        public abstract double DesignateMembershipGradeOfFuzzySet(double arg);

        public List<double> GetParameters()
        {
            return parameters;
        }

        public void SetParameters(List<double> parameters)
        {
            this.ValidateMembershipFunction(parameters);
            this.parameters = parameters;
        }
        public string DescribeMembershipFunction()
        {
            StringBuilder membershipFunctionDescribe = new StringBuilder();
            membershipFunctionDescribe.Append(TypeOfMembershipFunction + " function ");
            membershipFunctionDescribe.Append(this.Name);
            membershipFunctionDescribe.Append(" [");
            foreach (double parameter in parameters)
            {
                membershipFunctionDescribe.Append(parameter.ToString() + " ");
            }
            if(membershipFunctionDescribe[membershipFunctionDescribe.Length-1].Equals(' '))
            {
                membershipFunctionDescribe.Remove(membershipFunctionDescribe.Length - 1, 1);
            }
            membershipFunctionDescribe.Append(']');
            return membershipFunctionDescribe.ToString();            
        }

        virtual protected void ValidateMembershipFunction(List<double> parameters)
        {

            if(parameters.Count < this.NumberOfArguments)
            {
                throw new ArgumentException("It's too litle number parameter. Correct number of parameters is + " + NumberOfArguments.ToString());
            }
            else if (parameters.Count > this.NumberOfArguments)
            {
                throw new ArgumentException("It's too much number parameter. Correct number of parameters is + " + NumberOfArguments.ToString());
            }
            if (IncreasingFlag)
            {
                for(int i = 0; i < NumberOfArguments - 1; i++)
                {
                    if(parameters[i] > parameters[i + 1])
                    {
                        throw new ArgumentException("This type of membership functions have active increasing flag. Parameters have to be in ascending order.");
                    }
                }
            }
        }
    }
}
