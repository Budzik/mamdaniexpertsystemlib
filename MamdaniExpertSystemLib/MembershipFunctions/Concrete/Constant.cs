﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.MembershipFunctions.Concrete
{
    public class Constant : MembershipFunction
    {
        public Constant(List<double> parameters, string name) : base(parameters, name)
        {
        }

        public Constant()
        {
        }

        public override int NumberOfArguments { get { return 1; } }
        public override bool IncreasingFlag { get { return false; } }

        public override string TypeOfMembershipFunction
        {
            get
            {
                return "Constant";
            }
        }

        public override double DesignateMembershipGradeOfFuzzySet(double arg)
        {
            return parameters[0];
        }
        protected override void ValidateMembershipFunction(List<double> parameters)
        {
            base.ValidateMembershipFunction(parameters);
            if (parameters[0] < 0 || parameters[0] > 1)
            {
                throw new ArgumentException("This type of membership function can't accept argument greater then 1 and less then 0.");
            }

        }
    }
}
