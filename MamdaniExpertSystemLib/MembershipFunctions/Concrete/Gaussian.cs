﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.MembershipFunctions.Concrete
{
    public class Gaussian : MembershipFunction
    {
        public Gaussian(List<double> parameters, string name) : base(parameters, name)
        {
        }

        public Gaussian()
        {
        }

        public override int NumberOfArguments { get { return 2; } }
        public override bool IncreasingFlag { get { return false; } }
        public override string TypeOfMembershipFunction
        {
            get
            {
                return "Gaussian";
            }
        }
        public override double DesignateMembershipGradeOfFuzzySet(double arg)
        {
            return Math.Pow(Math.E, (-(1.0 / 2.0) * Math.Pow((arg - parameters[0]) / parameters[1], 2)));
        }
    }
}
