﻿using System.Collections.Generic;
using MamdaniExpertSystemLib.MembershipFunctions.Abstract;

namespace MamdaniExpertSystemLib.MembershipFunctions.Concrete
{
    public class Singleton : MembershipFunction
    {
        public Singleton(List<double> parameters, string name) : base(parameters, name)
        {
        }

        public Singleton()
        {
        }

        public override int NumberOfArguments { get { return 1; } }

        public override bool IncreasingFlag { get { return false; } }

        public override string TypeOfMembershipFunction
        {
            get
            {
                return "Singleton";
            }
        }
        public override double DesignateMembershipGradeOfFuzzySet(double arg)
        {
            if(arg != parameters[0])
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}
