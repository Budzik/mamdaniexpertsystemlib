﻿using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using MamdaniExpertSystemLib.MembershipFunctions.Concrete;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.MembershipFunctions
{
    public static class MembershipFunctionFactory
    {
        static public MembershipFunction CreateMembershipFunction(TypeOfMembershipFunctions membeshipFunction, List<double> parameters, string name)
        {
            switch (membeshipFunction)
            {
                case TypeOfMembershipFunctions.Constant:
                    return new Constant(parameters, name);
                    break;
                case TypeOfMembershipFunctions.Gaussian:
                    return new Gaussian(parameters, name);
                    break;
                case TypeOfMembershipFunctions.Singleton:
                    return new Singleton(parameters, name);
                    break;
                case TypeOfMembershipFunctions.Trapezium:
                    return new Trapezium(parameters, name);
                    break;
                case TypeOfMembershipFunctions.Triangle:
                    return new Triangle(parameters, name);
                    break;
                default:
                    throw new Exception("This type of exception is not exist.");
                    break;
            }
        }
        static public List<string> GiveAvailableTypeOfMembershipFunctions()
        {
            List<string> TypeOfMembershipFunctions = new List<string>();
            TypeOfMembershipFunctions temp;
            for(int i = 0; i != int.MaxValue; i++)
            {
                temp = (TypeOfMembershipFunctions)i;
                if (temp.ToString().Equals(i.ToString()))
                {
                    break;
                }
                TypeOfMembershipFunctions.Add(temp.ToString());
            }
            return TypeOfMembershipFunctions;
        }
        static public int GetNumberOfFunction(string Name)
        {
            for (int i = 0; i != int.MaxValue; i++)
            {
                TypeOfMembershipFunctions temp = (TypeOfMembershipFunctions)i;
                if (temp.ToString().Equals(Name))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
