﻿namespace MamdaniExpertSystemLib.MembershipFunctions
{
    public enum TypeOfMembershipFunctions
    {
        Constant,
        Gaussian,
        Singleton,
        Trapezium,
        Triangle
    }
}
