﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.MembershipFunctons.Abstract
{
    public abstract class MembershipFunction : IMembershipFunction
    {
        protected List<double> parameters;
        public abstract bool IncreasingFlag { get; }
        public string Name { get; set; }
        public abstract int NumberOfArguments { get; }
        public List<double> Parameters
        {
            get
            {
                return this.parameters;
            }

            set
            {
                this.ValidateMembershipFunction(value);
                parameters = value;
            }
        }

        public abstract double DesignateMembershipGradeOfFuzzySet(double arg);

        public List<double> GetParameters()
        {
            return parameters;
        }

        public void SetParameters(List<double> parameters)
        {
            this.ValidateMembershipFunction(parameters);
            this.parameters = parameters;
        }

        virtual protected void ValidateMembershipFunction(List<double> parameters)
        {

            if(parameters.Count < this.NumberOfArguments)
            {
                throw new ArgumentException("It's too litle number parameter. Correct number of parameters is + " + NumberOfArguments.ToString());
            }
            else if (parameters.Count > this.NumberOfArguments)
            {
                throw new ArgumentException("It's too much number parameter. Correct number of parameters is + " + NumberOfArguments.ToString());
            }
            if (IncreasingFlag)
            {
                for(int i = 0; i < NumberOfArguments - 1; i++)
                {
                    if(parameters[i] > parameters[i + 1])
                    {
                        throw new ArgumentException("This type of membership functions have active increasing flag. Parameters have to be in ascending order.");
                    }
                }
            }
        }
    }
}
