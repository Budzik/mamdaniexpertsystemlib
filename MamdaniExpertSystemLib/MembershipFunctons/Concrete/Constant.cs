﻿using MamdaniExpertSystemLib.MembershipFunctons.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.MembershipFunctons.Concrete
{
    public class Constant : MembershipFunction
    {
        public override int NumberOfArguments { get { return 1; } }
        public override bool IncreasingFlag { get { return false; } }
        public override double DesignateMembershipGradeOfFuzzySet(double arg)
        {
            return parameters[0];
        }
        protected override void ValidateMembershipFunction(List<double> parameters)
        {
            base.ValidateMembershipFunction(parameters);
            if (parameters[0] < 0 || parameters[0] > 1)
            {
                throw new ArgumentException("This type of membership function can't accept argument greater then 1 and less then 0.");
            }

        }
    }
}
