﻿using MamdaniExpertSystemLib.MembershipFunctons.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.MembershipFunctons.Concrete
{
    public class Gaussian : MembershipFunction
    {
        public override int NumberOfArguments { get { return 2; } }
        public override bool IncreasingFlag { get { return false; } }

        public override double DesignateMembershipGradeOfFuzzySet(double arg)
        {
            return Math.Pow(Math.E, (-(1.0 / 2.0) * Math.Pow((arg - parameters[0]) / parameters[1], 2)));
        }
    }
}
