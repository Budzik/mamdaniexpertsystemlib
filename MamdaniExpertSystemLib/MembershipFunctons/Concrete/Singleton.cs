﻿using MamdaniExpertSystemLib.MembershipFunctons.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.MembershipFunctons.Concrete
{
    public class Singleton : MembershipFunction
    {
        public override int NumberOfArguments { get { return 1; } }

        public override bool IncreasingFlag { get { return false; } }

        public override double DesignateMembershipGradeOfFuzzySet(double arg)
        {
            if(arg != parameters[0])
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}
