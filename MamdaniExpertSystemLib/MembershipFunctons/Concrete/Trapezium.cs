﻿using MamdaniExpertSystemLib.MembershipFunctons.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamdaniExpertSystemLib.MembershipFunctons.Concrete
{
    public class Trapezium : MembershipFunction
    {
        public override int NumberOfArguments { get { return 4; } }

        public override bool IncreasingFlag { get { return true; } }

        public override double DesignateMembershipGradeOfFuzzySet(double arg)
        {
            double result = 0;
            if (arg <= parameters[0] && arg >= parameters[3])
            {
                result = 0;
            }
            else if (arg >= parameters[0] && arg <= parameters[1])
            {
                if (parameters[0] < parameters[1])
                {
                    double a;
                    double b;
                    a = -1 / (parameters[0] - parameters[1]);
                    b = parameters[0] / (parameters[1] - parameters[2]);
                    result = a * arg + b;
                }
                else
                {
                    result = 1;
                }
            }
            else if(arg > parameters[1] && arg <= parameters[2])
            {
                result = 1;
            }
            else if (arg > parameters[2] && arg < parameters[3])
            {
                if (parameters[2] < parameters[3])
                {
                    double a;
                    double b;
                    a = -1 / (parameters[3] - parameters[2]);
                    b = parameters[3] / (parameters[3] - parameters[2]);
                    result = a * arg + b;
                }
                else
                {
                    result = 0;
                }
            }
            return result;
        }
    }
}
