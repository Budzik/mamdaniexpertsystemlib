﻿namespace MamdaniExpertSystemLib.Operators.OneArgumentOperators.Abstract
{
    public interface IOneArgumentOperator
    {
        double DesignateValue(double arg);
        void ValidateArguments(double arg);
    }
}
