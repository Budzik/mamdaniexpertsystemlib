﻿using System;

namespace MamdaniExpertSystemLib.Operators.OneArgumentOperators.Abstract
{
    public abstract class OneArgumentOperator : IOneArgumentOperator
    {
        public abstract double DesignateValue(double arg);
        public void ValidateArguments(double arg)
        {
            if(arg > 1 || arg < 0)
            {
                throw new ArgumentException("Argument is wrong. Argument must be range from 0 to 1");
            }
        }
    }
}
