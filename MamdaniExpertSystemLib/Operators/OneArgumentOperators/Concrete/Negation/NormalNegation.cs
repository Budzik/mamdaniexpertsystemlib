﻿using MamdaniExpertSystemLib.Operators.OneArgumentOperators.Abstract;

namespace MamdaniExpertSystemLib.Operators.OneArgumentOperators.Concrete.Negation
{
    public class NormalNegation : OneArgumentOperator
    {
        public override double DesignateValue(double arg)
        {
            this.ValidateArguments(arg);
            return -arg + 1;
        }
    }
}
