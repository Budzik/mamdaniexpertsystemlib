﻿namespace MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract
{
    public interface ITwoArgumentsOperator
    {
        double DesignateValue(double arg1, double arg2);
        void ValidateArguments(double arg1, double arg2);
    }
}
