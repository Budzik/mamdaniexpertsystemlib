﻿using System;

namespace MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract
{
    public abstract class TwoArgumentsOperator : ITwoArgumentsOperator
    {
        public abstract string Name { get; }
        public abstract double DesignateValue(double arg1, double arg2);
       
        public void ValidateArguments(double arg1, double arg2)
        {
            if ((arg1 > 1 || arg1 < 0) && (arg2 > 1 || arg2 < 0))
            {
                throw new ArgumentException("Both of argument is wrong. Arguments must be range from 0 to 1");
            }
            else if (arg1 > 1 || arg1 < 0 || arg2 > 1 || arg2 < 0)
            {
                throw new ArgumentException("One of argument is wrong. Arguments must be range from 0 to 1");
            }
        }
    }
}
