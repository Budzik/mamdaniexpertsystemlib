﻿using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using System;

namespace MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators
{
    public class MaxSNorm : TwoArgumentsOperator
    {
        public override string Name
        {
            get
            {
                return "Max";
            }
        }

        public override double DesignateValue(double arg1, double arg2)
        {
            this.ValidateArguments(arg1, arg2);
            return Math.Max(arg1, arg2);
        }
    }
}
