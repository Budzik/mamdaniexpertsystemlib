﻿using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using System;

namespace MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators
{
    public class SumSNorm : TwoArgumentsOperator
    {
        public override string Name
        {
            get
            {
                return "Sum";
            }
        }

        public override double DesignateValue(double arg1, double arg2)
        {
            this.ValidateArguments(arg1, arg2);
            return Math.Min(1, arg1 + arg2);
        }
    }
}
