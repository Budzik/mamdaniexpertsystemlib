﻿using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using System;

namespace MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.TNormOperators
{
    public class MinTNorm : TwoArgumentsOperator
    {
        public override string Name
        {
            get
            {
                return "Min";
            }
        }

        public override double DesignateValue(double arg1, double arg2)
        {
            this.ValidateArguments(arg1, arg2);
            return Math.Min(arg1, arg2);
        }
    }
}
