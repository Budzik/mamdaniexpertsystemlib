﻿using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;

namespace MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.TNormOperators
{
    public class MulTNorm : TwoArgumentsOperator
    {
        public override string Name
        {
            get
            {
                return "Mul";
            }
        }

        public override double DesignateValue(double arg1, double arg2)
        {
            this.ValidateArguments(arg1, arg2);
            return arg1 * arg2;
        }
    }
}
