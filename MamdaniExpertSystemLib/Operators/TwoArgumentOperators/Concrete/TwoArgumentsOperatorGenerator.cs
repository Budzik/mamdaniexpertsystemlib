﻿using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.TNormOperators;
using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete
{
    public static class TwoArgumentsOperatorGenerator
    {
        static public TwoArgumentsOperator GetSNorm(SNorms snorm)
        {
            switch (snorm)
            {
                case SNorms.Max:
                    return new MaxSNorm();
                    break;
                case SNorms.Sum:
                    return new SumSNorm();
                    break;
                default:
                    return null;
                    break;
            }
        }
        static public TwoArgumentsOperator GetTNorm(TNorms tnorm)
        {
            switch (tnorm)  
            {
                case TNorms.Min:
                    return new MinTNorm();
                    break;
                case TNorms.Mul:
                    return new MulTNorm();
                    break;
                default:
                    return null;
                    break;
            }
        }
        static public SNorms WhichSNorm(string text)
        {
            int i = 0;
            SNorms s = (SNorms)i;
            while (!(s.ToString().Equals(i.ToString())))
            {
                s = (SNorms)i;
                if (s.ToString().Equals(text))
                {
                    return s;
                }
                i++;
            }
            throw new ArgumentException("This type of SNorm is not exist");
        }
        static public TNorms WhichTNorm(string text)
        {
            int i = 0;
            TNorms s = (TNorms)i;
            while (!(s.ToString().Equals(i.ToString())))
            {
                s = (TNorms)i;
                if (s.ToString().Equals(text))
                {
                    return s;
                }
                i++;
            }
            throw new ArgumentException("This type of TNorm is not exist");
        }
        static public List<string> GiveAllAvailableSNorms()
        {
            List<string> SNormsList = new List<string>();
            SNorms temp;
            for (int i = 0; i != int.MaxValue; i++)
            {
                temp = (SNorms)i;
                if (temp.ToString().Equals(i.ToString()))
                {
                    break;
                }
                SNormsList.Add(temp.ToString());
            }
            return SNormsList;
        }
        static public List<string> GiveAllAvailableTNorms()
        {
            List<string> TNormsList = new List<string>();
            TNorms temp;
            for (int i = 0; i != int.MaxValue; i++)
            {
                temp = (TNorms)i;
                if (temp.ToString().Equals(i.ToString()))
                {
                    break;
                }
                TNormsList.Add(temp.ToString());
            }
            return TNormsList;
        }
        static public int GiveNumberOfSnorm(string name)
        {
            SNorms temp;
            for (int i = 0; i != int.MaxValue; i++)
            {
                temp = (SNorms)i;
                if (temp.ToString().Equals(name))
                {
                    return (int)temp;
                }
                if (temp.ToString().Equals(i.ToString()))
                {
                    break;
                }
            }
            throw new ArgumentException("This SNorm (" + name + ") it not exist");
        }
        static public int GiveNumberOfTNorm(string name)
        {
            TNorms temp;
            for (int i = 0; i != int.MaxValue; i++)
            {
                temp = (TNorms)i;
                if (temp.ToString().Equals(name))
                {
                    return (int)temp;
                }
                if (temp.ToString().Equals(i.ToString()))
                {
                    break;
                }
            }
            throw new ArgumentException("This TNorm (" + name + ") it not exist");
        }
    }
    
}
