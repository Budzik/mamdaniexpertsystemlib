﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MamdaniExpertSystemLib
{
    static public class ParametersVector
    {

        static public string TextFromParameters(List<double> parameters)
        {
            StringBuilder temp = new StringBuilder();
            temp.Append('[');
            foreach(double i in parameters)
            {
                temp.Append(i.ToString());
                temp.Append(' ');
            }
            temp.Remove(temp.Length - 1, 1);
            temp.Append(']');
            return temp.ToString();
        }
        static public List<double> ParametersFromText(string text)
        {
            string[] separators = new string[] { "[", "]", " " };
            string[] parts = text.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            List<double> temp = new List<double>();
            foreach(string part in parts)
            {
                temp.Add(double.Parse(part));
            }
            return temp;             
        }

    }
}
