﻿using System;
using System.Collections.Generic;

namespace MamdaniExpertSystemLib.Rules
{
    public class ElementOfExpression
    {
        public ElementOfExpression()
        {

        }
        public ElementOfExpression(TypeOfElement typeOfElement, List<int> parameters, double? value)
        {
            TypeOfElement = typeOfElement;
            Parameters = parameters;
            Value = value;
        }
        public ElementOfExpression(string text)
        {
            
        }
        public TypeOfElement TypeOfElement { get; set; }
        public List<int> Parameters { get; set; }
        public double? Value { get; set; }

        public override string ToString()
        {
            switch (TypeOfElement)
            {
                case TypeOfElement.InputVariable:
                    return "iv" + Parameters[0].ToString() + "a" + Parameters[1].ToString();
                    break;
                case TypeOfElement.Constant:
                    return Value.ToString();
                    break;
                case TypeOfElement.OutputVariable:
                    return "ov" + Parameters[0].ToString() + "a" + Parameters[1].ToString();
                    break;
                case TypeOfElement.TNorm:
                    return "*";
                    break;
                case TypeOfElement.SNorm:
                    return "+";
                    break;
                case TypeOfElement.OpeningBracket:
                    return "(";
                    break;
                case TypeOfElement.ClosingBracket:
                    return ")";
                    break;
                case TypeOfElement.Negation:
                    return "~";
                    break;
                default:
                    return "";
                    break;
            }
        }
        public static ElementOfExpression ElementOfExpressionFactory(string text)
        {
            string[] inputSeparators = new string[] { "iv", "a" };
            string[] outputSeparators = new string[] { "ov", "a" };
            ElementOfExpression elementOfExpression = new ElementOfExpression();
            string[] parametersInText;
            List<int> parameters;
            double value;
            switch (text[0])
            {
                case 'i':
                    parametersInText = text.Split(inputSeparators, StringSplitOptions.RemoveEmptyEntries);
                    parameters = new List<int>() { int.Parse(parametersInText[0]), int.Parse(parametersInText[1]) };
                    elementOfExpression.Parameters = parameters;
                    elementOfExpression.TypeOfElement = TypeOfElement.InputVariable;
                    break;
                case 'o':
                    parametersInText = text.Split(outputSeparators, StringSplitOptions.RemoveEmptyEntries);
                    parameters = new List<int>() { int.Parse(parametersInText[0]), int.Parse(parametersInText[1]) };
                    elementOfExpression.Parameters = parameters;
                    elementOfExpression.TypeOfElement = TypeOfElement.OutputVariable;
                    break;
                case '+':
                    elementOfExpression.TypeOfElement = TypeOfElement.SNorm;
                    break;
                case '*':
                    elementOfExpression.TypeOfElement = TypeOfElement.TNorm;
                    break;
                case '~':
                    elementOfExpression.TypeOfElement = TypeOfElement.Negation;
                    break;
                default:
                    value = double.Parse(text);
                    if(value > 1)
                    {
                        value = 1;
                    }
                    else if(value < 0)
                    {
                        value = 0;
                    }
                    elementOfExpression.TypeOfElement = TypeOfElement.Constant;
                    elementOfExpression.Value = value;
                    break;
            }
            return elementOfExpression;
        }
    }
}
