﻿using MamdaniExpertSystemLib.Operators.OneArgumentOperators.Abstract;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MamdaniExpertSystemLib.Rules
{
    public class Rule
    {
        public List<ElementOfExpression> Inputs { get; set; }
        public List<ElementOfExpression> Outputs { get; set; }
        private readonly string[] ifExpressionSeparators = new string[] { "if", "then" };
        private readonly string[] operators = new string[] { "(", ")", "+", "*", "~" };
        private readonly string[] inputSeparators = new string[] { "iv", "a" };
        private readonly string[] outputSeparators = new string[] { "ov", "a" };
        private readonly string[] comma = new string[] { ", ", "(", ")" };
        public Rule()
        {
            Inputs = new List<ElementOfExpression>();
            Outputs = new List<ElementOfExpression>();
        }
        public Rule(List<ElementOfExpression> inputs, List<ElementOfExpression> outputs)
        {
            Inputs = inputs;
            Outputs = outputs;
        }
        public string ShowRule()
        {
            StringBuilder rule = new StringBuilder();
            rule.Append("if(");
            rule.Append(ShowInput());
            rule.Append(")then(");
            rule.Append(ShowOutput());
            rule.Append(')');

            return rule.ToString();

        }
        public double DesignateValue(List<List<double>> inputsValues, TwoArgumentsOperator TNorm, TwoArgumentsOperator SNorm, OneArgumentOperator Negation)
        {
            Stack<double> es = new Stack<double>();
            foreach (ElementOfExpression item in Inputs)
            {
                if (IsOperator(item.ToString()))
                {
                    if (NumberOfArgument(item.ToString()) == 2)
                    {
                        double temp = es.Pop();
                        double temp1 = es.Pop();
                        double result;
                        if (item.ToString().Equals("+"))
                        {
                            result = SNorm.DesignateValue(temp1, temp);
                        }
                        else
                        {
                            result = TNorm.DesignateValue(temp1, temp);
                        }
                        es.Push(result);
                    }
                    else
                    {
                        double temp = es.Pop();
                        double result;
                        result = Negation.DesignateValue(temp);
                        es.Push(result);
                    }
                }
                else
                {
                    if (IsArgument(item.ToString()))
                    {
                        es.Push(inputsValues[item.Parameters[0]-1][item.Parameters[1]-1]);
                    }
                    else
                    {
                        es.Push((double)item.Value);
                    }
                    
                }
            }
            return es.Pop();
        }
        public void BuildRule(string expression)
        {
            Inputs.Clear();
            Outputs.Clear();
            expression = expression.ToLower();
            ValidateRule(expression);
            string[] expressionElement = expression.Split(ifExpressionSeparators, StringSplitOptions.RemoveEmptyEntries);
            BuildInput(expressionElement[0]);
            BuildOutput(expressionElement[1]);
        }
        public bool IsArgument(string expression)
        {
            if (expression[0] == 'i')
            {
                return true;
            }
            return false;
        }
        public void RepairRuleAfterDeleteInputMembershipFunction(int i, int j)
        {
            i++;
            j++;
            string s = ShowRule();
            string textToOperate = s.Split(ifExpressionSeparators, StringSplitOptions.RemoveEmptyEntries)[0];
            string nextText = s.Split(ifExpressionSeparators, StringSplitOptions.RemoveEmptyEntries)[1];

            string[] separetedText = textToOperate.Split(new string[] { "iv" + i.ToString() + "a" + j.ToString() }, StringSplitOptions.None);
            if (separetedText.Length > 1) {
                string ChangedText = "";
                List<double> NeutralElements = new List<double>();
                for (int w = 0; w < separetedText.Length - 1; w++)
                {
                    bool NegationFlag;
                    if (separetedText[w].Length != 0)
                    {
                        NegationFlag = separetedText[w][separetedText[w].Length - 1] == '~' ? true : false;
                    }
                    else
                    {
                        NegationFlag = false;
                    }
                    if (NegationFlag)
                    {
                        separetedText[w] = separetedText[w].Remove(separetedText[w].Length - 1);
                    }
                    int IDLeft = giveID(separetedText[0], true);
                    int IDRight = giveID(separetedText[1], false);
                    int neutralElement = NeutralElement(IDLeft, IDRight);
                    NeutralElements.Add(neutralElement);
                }
                int k = 0;
                ChangedText += "if";
                foreach (string item in separetedText)
                {
                    ChangedText += item;
                    if (k < separetedText.Length - 1)
                    {
                        ChangedText += NeutralElements[k].ToString();
                    }
                    k++;
                }
                ChangedText += "then" + nextText;
                BuildRule(ChangedText);

            }
            foreach (ElementOfExpression item in Inputs)
            {
                if (item.TypeOfElement == TypeOfElement.InputVariable)
                {
                    if(item.Parameters[0] == i)
                    {
                        if(item.Parameters[1] > j)
                        {
                            item.Parameters[i]--;
                        }
                    }
                }
            }


        }
        public void RepairRuleAfterDeleteOutputMembershipFunction(int i, int j)
        {
            i++;
            j++;
            List<ElementOfExpression> elementsToRemove = new List<ElementOfExpression>();
            foreach(ElementOfExpression item in Outputs)
            {
                if(item.TypeOfElement == TypeOfElement.OutputVariable)
                {
                    if(item.Parameters[0] == i)
                    {
                        if(item.Parameters[1] == j)
                        {
                            elementsToRemove.Add(item);
                        }
                        else if(item.Parameters[1] > j)
                        {
                            item.Parameters[1]--;
                        }
                    }
                }
            }
            foreach(ElementOfExpression item in elementsToRemove)
            {
                Outputs.Remove(item);
            }

        }
        public void RepairRuleAfterDeleteInputLinguisticVariable(int i)
        {
            int max = MaximumOfInputsSecondParameter();
            for (int w = 1; w <= max; w++)
            {
                RepairRuleAfterDeleteInputMembershipFunction(i, 0);
            }
            i++;
            foreach (ElementOfExpression item in Inputs)
            {
                if (item.TypeOfElement == TypeOfElement.InputVariable)
                {
                    if (item.Parameters[0] > i)
                    {
                        item.Parameters[0]--;
                    }
                }
            }
        }
        public void RepairRuleAfterDeleteOutputLinguisticVariable(int i)
        {
            i++;
            List<ElementOfExpression> elementsToRemove = new List<ElementOfExpression>();

            foreach (ElementOfExpression item in Outputs)
            {
                if (item.TypeOfElement == TypeOfElement.OutputVariable)
                {
                    if (item.Parameters[0] == i)
                    {
                        elementsToRemove.Add(item);
                    }
                    else if(item.Parameters[0] > i)
                    {
                        item.Parameters[0]--;
                    }

                }
            }
            foreach(ElementOfExpression item in elementsToRemove)
            {
                Outputs.Remove(item);
            }
        }
        private int MaximumOfInputsSecondParameter()
        {
            int max = 0;
            foreach (ElementOfExpression item in Inputs)
            {
                if (item.TypeOfElement == TypeOfElement.InputVariable)
                {
                    if (item.Parameters[1] > max)
                    {
                        max = item.Parameters[1];
                    }
                }
            }
            return max;
        }
        private int giveID(string s, bool isLeft)
        {
            if(s.Length == 0)
            {
                return 0;
            }
            char g;
            if (isLeft)
            {
                g = s[s.Length - 1];
            }
            else
            {
                g = s[0];
            }
            switch (g)
            {
                case '(':
                    return 1;
                case ')':
                    return 1;
                case '*':
                    return 2;
                case '+':
                    return 3;
                default:
                    return 0;
            }
        }
        private int NeutralElement(int i, int j)
        {
            int[,] NeutralElements = new int[,] { { 0, 0, 1, 0 }, { 0, 0, 1, 0 }, { 1, 1, 1, 1 }, { 0, 0, 1, 0 } };
            return NeutralElements[i, j];
        }
        private string ShowInput()
        {
            StringBuilder expression = new StringBuilder();
            Stack<string> es = new Stack<string>();
            foreach (ElementOfExpression item in Inputs)
            {
                if (IsOperator(item.ToString()))
                {
                    if (NumberOfArgument(item.ToString()) == 2)
                    {
                        string temp = es.Pop();
                        string temp1 = es.Pop();
                        if (OperatorsPriority(item.ToString()) >= PriorityOfExpression(temp))
                        {
                            temp = ExpresionWithBrackets(temp);
                        }
                        if (OperatorsPriority(item.ToString()) > PriorityOfExpression(temp1))
                        {
                            temp1 = ExpresionWithBrackets(temp1);
                        }
                        es.Push(temp1 + item.ToString() + temp);
                    }
                    else
                    {
                        string temp = es.Pop();
                        if (OperatorsPriority(item.ToString()) > PriorityOfExpression(temp))
                        {
                            temp = ExpresionWithBrackets(temp);
                        }
                        es.Push(item.ToString() + temp);
                    }
                }
                else
                {
                    es.Push(item.ToString());
                }
            }
            return es.Pop();
        }
        private string ShowOutput()
        {
            StringBuilder output = new StringBuilder();
            int i = 0;
            foreach (ElementOfExpression element in Outputs)
            {
                if (i != Outputs.Count - 1)
                {
                    output.Append(element.ToString());
                    output.Append(", ");
                }
                else
                {
                    output.Append(element.ToString());
                }
                i++;
            }
            return output.ToString();
        }
        private int NumberOfArgument(string o)
        {
            if (o.Equals("+") || o.Equals("*"))
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
        private string ExpresionWithBrackets(string expression)
        {
            return '(' + expression + ')';
        }
        private void BuildInput(string expression)
        {
            Stack<String> operatorStack = new Stack<string>();

            for (int i = 0; i < expression.Length; i++)
            {
                if (!operators.Contains<string>(expression[i].ToString()))
                {
                    StringBuilder argument = new StringBuilder();
                    while (!(IsOperator(expression[i + 1].ToString()) || expression[i + 1] == '(' || expression[i + 1] == ')'))
                    {
                        argument.Append(expression[i]);
                        i++;
                        if (i >= expression.Length)
                        {
                            break;
                        }
                    }
                    argument.Append(expression[i]);
                    ElementOfExpression elementOfExpression = ElementOfExpression.ElementOfExpressionFactory(argument.ToString());
                    Inputs.Add(elementOfExpression);
                }
                else
                {
                    if (IsOperator(expression[i].ToString()))
                    {
                        if (this.OperatorsPriority(expression[i].ToString()) <= this.OperatorsPriority(operatorStack.Peek()))
                        {
                            while (this.OperatorsPriority(expression[i].ToString()) <= this.OperatorsPriority(operatorStack.Peek()))
                            {
                                Inputs.Add(ElementOfExpression.ElementOfExpressionFactory(operatorStack.Pop()));
                            }
                            operatorStack.Push(expression[i].ToString());

                        }
                        else
                        {
                            operatorStack.Push(expression[i].ToString());
                        }
                    }
                    else if (expression[i] == '(')
                    {
                        operatorStack.Push(expression[i].ToString());
                    }
                    else if (expression[i] == ')')
                    {
                        while (!operatorStack.Peek().Equals("("))
                        {
                            Inputs.Add(ElementOfExpression.ElementOfExpressionFactory(operatorStack.Pop()));
                        }
                        operatorStack.Pop();
                    }
                }
            }
            while (operatorStack.Count > 0)
            {
                Inputs.Add(ElementOfExpression.ElementOfExpressionFactory(operatorStack.Pop()));
            }

        }
        private void BuildOutput(string expression)
        {
            string[] outputs = expression.Split(comma, StringSplitOptions.RemoveEmptyEntries);
            foreach(string output in outputs)
            {
                Outputs.Add(ElementOfExpression.ElementOfExpressionFactory(output));
            }
        }
        private bool IsOperator(string text)
        {
            if (text.Equals("+") || text.Equals("*") || text.Equals("~"))
            {
                return true;
            }
            return false;
        }
        private int OperatorsPriority(string text)
        {
            if (text.Equals("+"))
            {
                return 1;
            }
            else if (text.Equals("*"))
            {
                return 2;
            }
            else if (text.Equals("~"))
            {
                return 3;
            }
            return 0;
        }
        private void ValidateRule(string expression)
        {
            if (expression.Split(ifExpressionSeparators, StringSplitOptions.RemoveEmptyEntries).Length != 2)
            {
                throw new ArgumentException("Rule have to have format if[Expression]then[ListOfOutputs]");
            }
            string expressionString = expression.Split(ifExpressionSeparators, StringSplitOptions.RemoveEmptyEntries)[0];
            string outputsString = expression.Split(ifExpressionSeparators, StringSplitOptions.RemoveEmptyEntries)[1];
            int i = 0;
            foreach (char s in expressionString)
            {
                if (!(s == 'i' || s == 'v' || s == 'a' || s == '+' || s == '*' || s == '~' || (s >= '0' && s <= '9') || s == (CultureInfo.CurrentCulture.NumberFormat).CurrencyDecimalSeparator[0] || s == '(' || s == ')'))
                {
                    throw new ArgumentException("Expression contain wrong character in input expression. Wrong character is " + s.ToString() + " in position " + i.ToString());
                }
                i++;
            }
            i = 0;
            foreach (char s in outputsString)
            {
                if (!(s == 'o' || s == 'v' || s == 'a' || (s >= '0' && s <= '9') || s == '(' || s == ')' || s == ' ' || s == ','))
                {
                    throw new ArgumentException("Expression contain wrong character output expression. Wrong character is " + s.ToString() + " in position " + i.ToString());
                }
                i++;
            }
            string[] arguments = expressionString.Split(operators, StringSplitOptions.RemoveEmptyEntries);
            foreach (string argument in arguments)
            {
                try
                {
                    if (argument.Split(inputSeparators, StringSplitOptions.RemoveEmptyEntries).Length == 2)
                    {
                        ElementOfExpression eoe = ElementOfExpression.ElementOfExpressionFactory(argument);
                    }
                    else if (argument.Split((CultureInfo.CurrentCulture.NumberFormat).CurrencyDecimalSeparator[0]).Length == 2)
                    {
                        ElementOfExpression eoe = ElementOfExpression.ElementOfExpressionFactory(argument);
                    }
                    else if (argument.Equals("1") || argument.Equals("0"))
                    {
                        ElementOfExpression eoe = ElementOfExpression.ElementOfExpressionFactory(argument);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                catch (Exception e)
                {
                    throw new ArgumentException("One of element of expression is wrong. Wrong is element: " + argument);
                }
            }
            string[] outputs = outputsString.Split(comma, StringSplitOptions.RemoveEmptyEntries);
            foreach (string output in outputs)
            {
                try
                {
                    ElementOfExpression eoe = ElementOfExpression.ElementOfExpressionFactory(output);
                }
                catch
                {
                    throw new ArgumentException("One of output is wrong. Wrong is element: " + output);
                }
            }
            try
            {
                int notClosedBracket = 0;
                foreach (char s in expression)
                {
                    if (s == '(')
                    {
                        notClosedBracket++;
                    }
                    if(s == ')')
                    {
                        notClosedBracket--;
                    }
                    if(notClosedBracket < 0)
                    {
                        throw new Exception();
                    }
                }
                if(notClosedBracket != 0)
                {
                    throw new Exception();
                }
            }
            catch
            {
                throw new ArgumentException("Not all bracket have been opened or closed");
            }
        }
        private int PriorityOfExpression(string expression)
        {
            int notClosedBracket = 0;
            int priority = 5;
            int currentPriority = 5;
            int minimum = 5;
            foreach(char s in expression)
            {
                if (IsOperator(s.ToString()))
                {
                    if(notClosedBracket > 0)
                    {
                        currentPriority = 5;
                    }
                    else
                    {
                        currentPriority = OperatorsPriority(s.ToString());
                    }
                }
                if(s == '(')
                {
                    notClosedBracket++;
                }
                if(s == ')')
                {
                    notClosedBracket--;
                }
                if(currentPriority < minimum)
                {
                    minimum = currentPriority;
                }
            }
            priority = minimum;
            return priority;
        }
    }
}
