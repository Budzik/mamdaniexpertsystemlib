﻿using MamdaniExpertSystemLib.LinguisticVariables.Concrete;
using MamdaniExpertSystemLib.MembershipFunctions.Abstract;
using System;
using System.Collections.Generic;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Abstract;
using MamdaniExpertSystemLib.Operators.OneArgumentOperators.Abstract;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.TNormOperators;
using MamdaniExpertSystemLib.Operators.TwoArgumentOperators.Concrete.SNormOperators;
using MamdaniExpertSystemLib.Operators.OneArgumentOperators.Concrete.Negation;
using System.Globalization;

namespace MamdaniExpertSystemLib.Rules
{
    public class RuleSet
    {
        private List<Rule> Rules;
        public TwoArgumentsOperator TNorm { get; set; }
        public TwoArgumentsOperator SNorm { get; set; }
        public TwoArgumentsOperator Aggregation { get; set; }
        public OneArgumentOperator Negation { get; set; }
        public List<LinguisticVariable> Inputs { get; set; }
        public List<LinguisticVariable> Outputs { get; set; }
        public int RulesNumber { get { return Rules.Count; } }
        private readonly string[] operators = new string[] { "*", "+", "~", "(", ")" };
        private readonly string[] ifThen = new string[] { "if", "then" };
        private readonly string[] equalSign = new string[] { " = ", " =", "= ", "=", " " };
        private readonly string[] outputSeparators = new string[] { ", ", ",", " , ", " ," , "(", ")" };        
        public RuleSet()
        {
            Rules = new List<Rule>();
            TNorm = new MinTNorm();
            SNorm = new MaxSNorm();
            Aggregation = new MaxSNorm();
            Negation = new NormalNegation();
        }
        public RuleSet(List<LinguisticVariable> inputs, List<LinguisticVariable> outputs)
        {
            Rules = new List<Rule>();
            TNorm = new MinTNorm();
            SNorm = new MaxSNorm();
            Negation = new NormalNegation();
            Aggregation = new MaxSNorm();
            this.Outputs = outputs;
            this.Inputs = inputs;
        }
        public void AddRule(string expression)
        {
            string normalizeExpression = this.ParseReadableRuleToRule(expression);
            Rule rule = new Rule();
            rule.BuildRule(normalizeExpression);
            Rules.Add(rule);
        }
        public List<string> ShowAllRule()
        {
            List<string> temp = new List<string>();
            foreach (Rule item in Rules)
            {
                temp.Add(this.ParseRuleToReadableRule(item.ShowRule()));
            }
            return temp;
        }
        public void EditRule(string expression, int number)
        {
            string normalizeExpression = this.ParseReadableRuleToRule(expression);
            Rule rule = new Rule();
            rule.BuildRule(normalizeExpression);
            Rules[number] = rule;
        }
        public void RemoveRule(int number)
        {
            Rules.RemoveAt(number);
        }
        public List<List<double>> AggragateRules(List<double> args)
        {
            List<List<double>> outputsValue = new List<List<double>>();
            List<List<double>> arguments = new List<List<double>>();
            foreach (LinguisticVariable output in Outputs)
            {
                List<double> zerosVector = new List<double>();
                foreach (MembershipFunction membershipFunction in output.MembershipFunctions)
                {
                    zerosVector.Add(0);
                }
                outputsValue.Add(zerosVector);
            }
            int i = 0;
            foreach (LinguisticVariable input in Inputs)
            {
                List<double> inputs = new List<double>();
                foreach (MembershipFunction membershipFunction in input.MembershipFunctions)
                {
                    inputs.Add(membershipFunction.DesignateMembershipGradeOfFuzzySet(args[i]));
                }
                arguments.Add(inputs);
                i++;
            }
            List<double> values = new List<double>();
            foreach(Rule rule in Rules)
            {
                values.Add(rule.DesignateValue(arguments, TNorm, SNorm, Negation));
            }
            for (int j = 0; j < values.Count; j++)
            {
                double value = values[j];
                foreach(ElementOfExpression item in Rules[j].Outputs)
                {
                    outputsValue[item.Parameters[0] - 1][item.Parameters[1] - 1] = Aggregation.DesignateValue(outputsValue[item.Parameters[0] - 1][item.Parameters[1] - 1], values[j]);
                }
            }
            return outputsValue;
        }
        private string ParseReadableRuleToRule(string rule)
        {
            string example;
            example = rule.ToLower();
            example = example.Replace(" and ", "*");
            example = example.Replace(" or ", "+");
            example = example.Replace("not ", "~");
            example = example.Replace("(not ", "(~");
            example = example.Replace("not(", "~(");
            string[] splitedExample = example.Split(ifThen, StringSplitOptions.RemoveEmptyEntries);
            string[] arguments = splitedExample[0].Split(operators, StringSplitOptions.RemoveEmptyEntries);
            string[] outputList = splitedExample[1].Split(outputSeparators, StringSplitOptions.RemoveEmptyEntries);
            List<ElementOfExpression> elements = new List<ElementOfExpression>();
            List<ElementOfExpression> outputsElements = new List<ElementOfExpression>();
            foreach (string item in arguments)
            {
                double number;
                try
                {
                    number = double.Parse(item);
                    elements.Add(ElementOfExpression.ElementOfExpressionFactory(number.ToString()));
                }
                catch
                {
                    List<int> temp1 = getMembershipFunctionPositionFromText(item, Inputs);
                    elements.Add(ElementOfExpression.ElementOfExpressionFactory("iv" + temp1[0] + "a" + temp1[1]));
                }
            }
            for(int i = 0; i < arguments.Length; i++)
            {
                splitedExample[0] = splitedExample[0].Replace(arguments[i], elements[i].ToString());
            }
            foreach (string item in outputList)
            {
                List<int> temp1 = getMembershipFunctionPositionFromText(item, Outputs);
                outputsElements.Add(ElementOfExpression.ElementOfExpressionFactory("ov" + temp1[0] + "a" + temp1[1]));
            }
            for (int i = 0; i < outputList.Length; i++)
            {
                splitedExample[1] = splitedExample[1].Replace(outputList[i], outputsElements[i].ToString());
            }
            return "if" + splitedExample[0] + "then" + splitedExample[1];           
        }
        private string ParseRuleToReadableRule(string rule)
        {
            string example = rule.ToLower();
            string temp;
            temp = example.Replace("+", " or ");
            temp = temp.Replace("*", " and ");
            temp = temp.Replace("~", "not ");
            string[] splitedExample = example.Split(ifThen, StringSplitOptions.RemoveEmptyEntries);
            string[] splitedTemp = temp.Split(ifThen, StringSplitOptions.RemoveEmptyEntries);
            string[] arguments = splitedExample[0].Split(operators, StringSplitOptions.RemoveEmptyEntries);
            string[] outputList = splitedExample[1].Split(outputSeparators, StringSplitOptions.RemoveEmptyEntries);
            List<string> TextsToReplace = new List<string>();
            foreach(string item in arguments)
            {
                double number;
                try
                {
                    number = double.Parse(item);
                    TextsToReplace.Add(number.ToString());
                }
                catch
                {
                    ElementOfExpression temp1 = ElementOfExpression.ElementOfExpressionFactory(item);
                    TextsToReplace.Add(getLinguisticVariableAndMembershipFunctionFromPosition(temp1.Parameters, Inputs));
                }
            }
            for (int i = 0; i < arguments.Length; i++)
            {
                splitedTemp[0] = splitedTemp[0].Replace(arguments[i], TextsToReplace[i]);
            }
            TextsToReplace.Clear();
            foreach(string item in outputList)
            {
                ElementOfExpression temp1 = ElementOfExpression.ElementOfExpressionFactory(item);
                TextsToReplace.Add(getLinguisticVariableAndMembershipFunctionFromPosition(temp1.Parameters, Outputs));
            }
            for (int i = 0; i < outputList.Length; i++)
            {
                splitedExample[1] = splitedExample[1].Replace(outputList[i], TextsToReplace[i]);
            }
            return "if" + splitedTemp[0] + "then" + splitedExample[1];
        }
        private List<int> getMembershipFunctionPositionFromText(string s, List<LinguisticVariable> linguisticVariables)
        {
            string[] temp = s.Split(equalSign, StringSplitOptions.RemoveEmptyEntries);
            int i = 0;
            foreach (LinguisticVariable item in linguisticVariables)
            {
                if (temp[0].Equals(item.Name))
                {
                    return new List<int> { i + 1, item.FindNumberOfMembershipFunction(temp[1]) + 1 };
                }
                i++;
            }
            throw new ArgumentException("Input linguistic variable named " + temp[0] + " is not exist.");
        }
        private string getLinguisticVariableAndMembershipFunctionFromPosition(List<int> parameters, List<LinguisticVariable> linguisticVariables)
        {
            return linguisticVariables[parameters[0]-1].Name + " = " + linguisticVariables[parameters[0]-1].MembershipFunctions[parameters[1] - 1].Name;
        }
        public void RepairAfterDeleteInputFunction(int lvNumber, int mfNumber)
        {
            foreach (Rule rule in Rules)
            {
                rule.RepairRuleAfterDeleteInputMembershipFunction(lvNumber, mfNumber);
            }
        }
        public void RepairAfterDeleteOutputFunction(int lvNumber, int mfNumber)
        {
            foreach (Rule rule in Rules)
            {
                rule.RepairRuleAfterDeleteOutputMembershipFunction(lvNumber, mfNumber);
            }
        }
        public void RepairAfterDeleteInputLinguisticVariable(int number)
        {
            foreach (Rule rule in Rules)
            {
                rule.RepairRuleAfterDeleteInputLinguisticVariable(number);
            }
        }
        public void RepairAfterDeleteOutputLinguisticVariable(int number)
        {
            foreach (Rule rule in Rules)
            {
                rule.RepairRuleAfterDeleteOutputLinguisticVariable(number);
            }
        }

    }
}
