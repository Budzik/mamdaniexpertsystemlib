﻿namespace MamdaniExpertSystemLib.Rules
{
    public enum TypeOfElement
    {
        InputVariable, Constant, SNorm, TNorm, Negation, OpeningBracket, ClosingBracket, OutputVariable
    }
}
